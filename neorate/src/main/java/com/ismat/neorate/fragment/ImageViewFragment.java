package com.ismat.neorate.fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ismat.neorate.R;

/**
 * A simple {@link Fragment} subclass for displaying a full screen image
 * 
 */
public class ImageViewFragment extends Fragment {

	private Integer itemData;
	private Bitmap myBitmap;
	public ProgressDialog pd;
	private ImageView ivImage;

	public ImageViewFragment() {
		// Required empty public constructor
	}

	public static ImageViewFragment newInstance() {
		ImageViewFragment f = new ImageViewFragment();
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_image_view, container,
				false);
		ivImage = (ImageView) root.findViewById(R.id.ivImageView);
		setImageInViewPager();
		return root;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (myBitmap != null) {
			myBitmap.recycle();
			myBitmap = null;
		}
	}

	public void setImageInViewPager() {
		try {
			// if image size is too large. Need to scale as below code.
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			myBitmap = BitmapFactory.decodeResource(getResources(), itemData,
					options);
			if (options.outWidth > 3000 || options.outHeight > 2000) {
				options.inSampleSize = 4;
			} else if (options.outWidth > 2000 || options.outHeight > 1500) {
				options.inSampleSize = 3;
			} else if (options.outWidth > 1000 || options.outHeight > 1000) {
				options.inSampleSize = 2;
			}
			options.inJustDecodeBounds = false;
			myBitmap = BitmapFactory.decodeResource(getResources(), itemData,
					options);
			if (myBitmap != null) {
				try {
					if (ivImage != null) {
						ivImage.setImageBitmap(myBitmap);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			System.gc();
		}
	}

	public void setImageList(Integer integer) {
		this.itemData = integer;
	}
}
