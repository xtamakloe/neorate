package com.ismat.neorate.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ismat.neorate.R;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.model.User;

/**
 * Fragment that handles login process for the application
 * 
 *
 */
public class SignInFragment extends Fragment {
	private SessionManager mSession;
	private AutoCompleteTextView mUsernameView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mProgressView;
	private OnFragmentInteractionListener mListener;

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 */
	public interface OnFragmentInteractionListener {
		public void beginSession(SessionManager sessionMgr, String username,
				String token, long userId);

		public void showProgress(boolean show, View loginView, View progressView);

		public boolean validateForm(TextView usernameView, TextView passwordView);
	}

	public static SignInFragment newInstance() {
		return new SignInFragment();
	}

	public SignInFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get session
		mSession = new SessionManager(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_sign_in, container,
				false);

		// Set up the login form
		mUsernameView = (AutoCompleteTextView) view.findViewById(R.id.username);

		mPasswordView = (EditText) view.findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.sign_in || id == EditorInfo.IME_NULL) {
							attemptSignIn();
							return true;
						}
						return false;
					}
				});

		Button mUsernameSignInButton = (Button) view
				.findViewById(R.id.username_sign_in_button);
		mUsernameSignInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptSignIn();
			}
		});

		mLoginFormView = view.findViewById(R.id.login_form);
		mProgressView = view.findViewById(R.id.login_progress);

		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
     *
     */
	public void attemptSignIn() {
		if (mListener.validateForm(mUsernameView, mPasswordView)) {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mListener.showProgress(true, mLoginFormView, mProgressView);

			doSignInTask(mUsernameView.getText().toString(), mPasswordView
					.getText().toString());
		}
	}

	private void doSignInTask(String username, String password) {
		mListener.showProgress(false, mLoginFormView, mProgressView);

		User user = User.findByUsername(username);
		if (user != null) {
			// check if user passwords match
			if (user.isValidPassword(password)) {
				mListener.beginSession(mSession, username, "token", user.getId());
			} else {
				Toast.makeText(getActivity(), "Invalid Password!",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getActivity(), "Invalid Username!", Toast.LENGTH_SHORT)
					.show();
		}
	}
}
