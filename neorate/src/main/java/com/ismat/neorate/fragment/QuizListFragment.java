package com.ismat.neorate.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.QuizDetailsActivity;
import com.ismat.neorate.adapter.QuizListsAdapter;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.model.Quiz;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple fragment that displays a list of quizzes
 * 
 *
 */
public class QuizListFragment extends Fragment {

	private static final String ARG_QUIZ_MODE = "type";
	private static final String ARG_ASSESSMENT_TYPE = "assessment_type";

	private ListView mListView;
	private ProgressBar mProgressBar;
	private TextView mEmptyView;
	private ArrayList<Quiz> mQuizzes;
	private QuizListsAdapter mAdapter;
	private SessionManager mSessionManager;

	public QuizListFragment() {
	}

	public static QuizListFragment newInstance(String quizMode,
			String assessmentType) {
		QuizListFragment fragment = new QuizListFragment();

		Bundle args = new Bundle();
		args.putString(ARG_QUIZ_MODE, quizMode);
		args.putString(ARG_ASSESSMENT_TYPE, assessmentType);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_quiz_lists, container,
				false);

		mListView = (ListView) view.findViewById(R.id.lv_quizzes_list);
		mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
		mEmptyView = (TextView) view.findViewById(R.id.tv_empty_quiz_list);

		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Show results screen
				Quiz quiz = (Quiz) mAdapter.getItem(position);
				Intent intent = new Intent(getActivity(),
						QuizDetailsActivity.class);
				Bundle bundle = new Bundle();
				bundle.putLong("quizId", quiz.getId());
				intent.putExtras(bundle);
				getActivity().startActivity(intent);
			}
		});

		mQuizzes = new ArrayList<Quiz>();
		mAdapter = new QuizListsAdapter(getActivity(), mQuizzes);
		mListView.setAdapter(mAdapter);

		loadQuizzes();

		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mSessionManager = new SessionManager(activity);
	}

	@Override
	public void onResume() {
		loadQuizzes();
		super.onResume();
	}

	private void loadQuizzes() {
		mProgressBar.setVisibility(View.VISIBLE);

		List<Quiz> quizzes = Quiz.findAll(Long.toString(mSessionManager
				.getLoggedInUserId()), getArguments().getString(ARG_QUIZ_MODE),
				getArguments().getString(ARG_ASSESSMENT_TYPE));

		mProgressBar.setVisibility(View.GONE);

		mQuizzes.clear();
		mQuizzes.addAll(quizzes);
		refreshView(mQuizzes, mListView, mEmptyView);

		mAdapter.notifyDataSetChanged();
	}

	private void refreshView(List<?> list, ListView listView, TextView emptyView) {
		if (list.size() > 0) {
			listView.setVisibility(View.VISIBLE);
			emptyView.setVisibility(View.GONE);
		} else {
			listView.setVisibility(View.GONE);
			emptyView.setVisibility(View.VISIBLE);
		}
	}
}
