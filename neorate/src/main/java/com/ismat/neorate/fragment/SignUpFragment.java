package com.ismat.neorate.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ismat.neorate.R;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.model.User;

/**
 * Fragment that handles sign up process for the application
 * 
 *
 */
public class SignUpFragment extends Fragment {

	private SignInFragment.OnFragmentInteractionListener mListener;
	private SessionManager mSession;
	private AutoCompleteTextView mUsernameView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mProgressView;

	public static SignUpFragment newInstance() {
		return new SignUpFragment();
	}

	public SignUpFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set session
		mSession = new SessionManager(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_sign_up, container,
				false);

		// Set up the login form.
		mUsernameView = (AutoCompleteTextView) view
				.findViewById(R.id.signup_username);

		mPasswordView = (EditText) view.findViewById(R.id.signup_password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.sign_up || id == EditorInfo.IME_NULL) {
							attemptSignUp();
							return true;
						}
						return false;
					}
				});

		Button mUsernameSignInButton = (Button) view
				.findViewById(R.id.username_sign_up_button);
		mUsernameSignInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptSignUp();
			}
		});

		mLoginFormView = view.findViewById(R.id.signup_form);
		mProgressView = view.findViewById(R.id.signup_progress);

		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (SignInFragment.OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * Attempts to register the account specified by the login form. If there
	 * are form errors (invalid username, missing fields, etc.), the errors are
	 * presented and no actual login attempt is made.
	 */
	public void attemptSignUp() {
		if (mListener.validateForm(mUsernameView, mPasswordView)) {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mListener.showProgress(true, mLoginFormView, mProgressView);
			doSignUpTask(mUsernameView.getText().toString(), mPasswordView
					.getText().toString());
		}
	}

	/**
     *
     */
	private void doSignUpTask(String username, String password) {
		mListener.showProgress(false, mLoginFormView, mProgressView);
		// Create new user if one doesn't currently exists with credentials
		// provided. 
		User user = User.findByUsername(username);
		if (user == null) {
			User newUser = new User();
			newUser.username = username;
			newUser.setPassword(password);
			
			// Make user with username adminxx an admin user
			if (username.matches("admin\\d*$"))	
				newUser.isAdmin = true; 
			
			newUser.save();

			mListener.beginSession(mSession, username, "token", newUser.getId());
		} else {
			Toast.makeText(getActivity(), "User already exists!",
					Toast.LENGTH_SHORT).show();
		}
	}
}
