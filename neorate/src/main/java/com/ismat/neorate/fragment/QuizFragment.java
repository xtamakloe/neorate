package com.ismat.neorate.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.QuizDetailsActivity;
import com.ismat.neorate.dialog.InputHeartRateBPMDialog;
import com.ismat.neorate.dialog.InputHeartRateBPMDialog.InputHeartRateBPMDialogListener;
import com.ismat.neorate.dialog.InputHeartRateCategoryDialog;
import com.ismat.neorate.dialog.InputHeartRateCategoryDialog.InputHeartRateCategoryDialogListener;
import com.ismat.neorate.dialog.QuizSetupDialog;
import com.ismat.neorate.dialog.QuizSetupDialog.QuizSetupDialogListener;
import com.ismat.neorate.helper.FlashingTimer;
import com.ismat.neorate.helper.HeartBeatPlayer;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.helper.Utility;
import com.ismat.neorate.model.Assessment;
import com.ismat.neorate.model.Quiz;
import com.ismat.neorate.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Simple {@link Fragment} that handles the different quizzes that the
 * application provides. Activities that contain this fragment must implement
 * the {@link QuizFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link QuizFragment#newInstance} factory method
 * to create an instance of this fragment.
 */
public class QuizFragment extends Fragment implements View.OnClickListener,
        InputHeartRateBPMDialogListener, InputHeartRateCategoryDialogListener,
        QuizSetupDialogListener {


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated to
     * the activity and potentially other fragments contained in that activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

//	public static final String ARG_FLASH_INTERVAL = "flash_inteval";
//	public static final String ARG_SHOW_TIMER = "show_timer";

    private static final String ARG_QUIZ_MODE = "quiz_mode";
    private static final String ARG_TIMER_MODE = "timer_mode";

    public static final int HR_ACTUAL_BPM = 1000;
    public static final int HR_BPM_CATEGORY = 1100;
    public static final int QUIZ_SETUP_DIALOG = 1200;

    public static final int QUIZ_TYPE_STANDARD = 1600;
    public static final int QUIZ_TYPE_ADVANCED = 1700;
    public static final int QUIZ_TYPE_RANDOM = 1800;
    public static final String QUIZ_MODE_QUIZ = "quiz";
    public static final String QUIZ_MODE_TRAINING = "training";

    public static final String HR_CATEGORY_BELOW_60 = "_60";
    public static final String HR_CATEGORY_60_100 = "60_100";
    public static final String HR_CATEGORY_OVER_100 = "100_";

    private OnFragmentInteractionListener mListener;
    private ImageButton mBtnAssessment;
    private Button mBtnRestartQuiz;
    private Button mBtnQuitQuiz;
    // private SoundTouchPlayable mFxPlayer;
//    private MediaPlayer mSoundPlayer;
    private HeartBeatPlayer mPlayer;
    private FrameLayout mFrameLayout;
    private LinearLayout mClockLayout;
    private LinearLayout mClockLayoutLeftColumn;
    private LinearLayout mClockLayoutRightColumn;
    private Chronometer mChronometer;


    // private boolean mQuizInSession;
    private boolean mAssessmentInSession;
    //private boolean mShowTimer;
    /* Mode to run quiz in: training or quiz */
    private String mQuizMode;
    /* Mode to run timer: clock, timer, both */
    private String mTimerMode;

    //private boolean mRandomizeTimer;
    private int mHeartRateBPM;
    private String mHeartRateCategory;
    private int mAssessmentType;
    private int mAssessmentCount;
    private int mClockAssessmentsCount;
    private int mFTimerAssessmentsCount;
    private int mAssessmentCounter = 1;
    private long mElapsedTime = 0;
    private long mStartTime;
    //	private int mFlashInterval;
    private List<Assessment> mAssessmentList;

    private Quiz mQuiz;
    private Assessment mCurrentAssessment;
    private FlashingTimer mFlashingTimer;

    private SessionManager mSessionManager;

    /**
     * This factory creates a new instance of this fragment
     *
     * @param quizMode  specifies mode to launch quiz in. Either "training" or "quiz"
     * @param timerMode specifies whether quiz should show a timer
     * @return A new instance of fragment QuizFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuizFragment newInstance(String quizMode, String timerMode) {
        QuizFragment fragment = new QuizFragment();
        Bundle args = new Bundle();

        args.putString(ARG_QUIZ_MODE, quizMode); // training, quiz
        args.putString(ARG_TIMER_MODE, timerMode); // clock, timer, both

        fragment.setArguments(args);
        return fragment;
    }

    public QuizFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuizMode = getArguments().getString(ARG_QUIZ_MODE);
            mTimerMode = getArguments().getString(ARG_TIMER_MODE);
        }
        // Show setup dialog
        showQuizSetupDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Buttons
        mBtnAssessment = (ImageButton) view.findViewById(R.id.btn_assessment);
        mBtnQuitQuiz = (Button) view.findViewById(R.id.btn_quit_quiz);
        mBtnRestartQuiz = (Button) view.findViewById(R.id.btn_restart_quiz);
        mBtnRestartQuiz.setOnClickListener(this);
        mBtnQuitQuiz.setOnClickListener(this);
        mBtnAssessment.setOnClickListener(this);

        // Set main layout to black
        mFrameLayout = (FrameLayout) view.findViewById(R.id.framelayout_quiz);
        mFrameLayout.setBackgroundColor(Color.BLACK);

        // Digital clock layout
        mClockLayout = (LinearLayout) view.findViewById(R.id.clock_layout);
        mClockLayoutLeftColumn = (LinearLayout) view
                .findViewById(R.id.left_column);
        mClockLayoutRightColumn = (LinearLayout) view
                .findViewById(R.id.right_column);
//        mClockLayoutRightColumn.setBackgroundColor(Color.WHITE);

        // Set Resuscitaire image to left column and white background for right
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            // For version < 16 (JB)
            mClockLayoutLeftColumn.setBackgroundDrawable(getResources()
                    .getDrawable(R.drawable.rescuscitaire_image));
        } else {
            // For version >= 16
            mClockLayoutLeftColumn.setBackground(getResources()
                    .getDrawable(R.drawable.rescuscitaire_image));
        }

        // Set up digital clock
        mChronometer = (Chronometer) view.findViewById(R.id.chronometer);
        mChronometer.setTypeface(Typeface.createFromAsset(getActivity()
                .getAssets(), "fonts/quartz.ttf"));
        mChronometer.setTextColor(Color.rgb(0, 128, 0));
        mChronometer.setBackgroundColor(Color.BLACK);

        // Initial View: Hide entire left column + clock on the right
        mClockLayoutLeftColumn.setVisibility(View.GONE);
        mChronometer.setVisibility(View.INVISIBLE);

        // Set up player
        mPlayer = new HeartBeatPlayer(getActivity(), mBtnAssessment);
    }

    @Override
    public void onPause() {
        super.onPause();

        // TODO: save session on pause, which session variables?
        endAssessment(false);

        endQuiz(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_assessment:
                runAssessments();
                break;
            case R.id.btn_restart_quiz:
                // Clear assessment data and restart quiz
                showRestartQuizDialog();
                break;
            case R.id.btn_quit_quiz:
                // Clear assessment data and exit quiz
                showQuitQuizDialog();
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Initialize session on when activity is connected
        mSessionManager = new SessionManager(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

	/* ASSESSMENTS */

    /**
     * Manages a quiz session. Runs a number of assessments and keeps track of
     * total grades for the session
     */
    void runAssessments() {
        // set start quiz on first assessment
        // if (!mQuizInSession)
        // startQuiz();
        if (mQuiz == null)
            startQuiz();

        // if assessment is not running start it, else display popup
        if (!mAssessmentInSession) {
            startAssessment();
        } else {
            // Display popup for assessment
            showInputHeartRateDialog(mAssessmentType);
        }
    }

    /**
     * Continue with quiz after user's response is graded
     */
    public void proceedToNextAssessment() {
        // End current assessment
        endAssessment(true);

        // Increase successful assessment counter
        mAssessmentCounter++;

        // Determine whether to end quiz or continue based on settings
        if (mAssessmentCounter <= mAssessmentCount) {
            startAssessment();
        } else {
            // End quiz successfully
            endQuiz(true);
        }
    }

    /**
     * Starts an assessment
     */
    void startAssessment() {
        mAssessmentInSession = true;

        // Create assessment
        mCurrentAssessment = new Assessment();

        // Set mode of timer to be used for assessment
        setAssessmentTimer(mCurrentAssessment);

        // Generate random heart rate and set category
        mHeartRateBPM = generateRandomHeartRate();
        mHeartRateCategory = getHeartRateCategory(mHeartRateBPM);

        // Play lub-dub sound for the heart rate
//        playSoundFX(mHeartRateBPM);
        mPlayer.playSoundFX(mHeartRateBPM);

        // Reset timer
        resetTiming();

        // Start timer
        startTiming();
    }

    /**
     * Stops current assessment session
     *
     * @param isCompleted specifies if assessment was completed before being ended
     */
    private void endAssessment(boolean isCompleted) {
        // Mark assessment stopped
        mAssessmentInSession = false;

        // Add assessment to quiz assessments if it was completed
        if (isCompleted) {
            setupAssessmentRecord();
        }
        // Clear current assessment
        mCurrentAssessment = null;

        // Stop sound
//        stopSoundFX();
        mPlayer.stopSoundFX();

        // Reset timer
        resetTiming();

        // Clean files and session variables
        // cleanup();
    }

    /**
     * Pauses an assessment. Keeps assessment session intact.
     */
    private void pauseAssessment() {
        // pause sound
//        pauseSoundFX();
        mPlayer.pauseSoundFX();

        // Pause timer
        stopTiming();
    }

    /**
     * Resumes a paused assessment
     */
    private void resumeAssessment() {
        // Resume playing sound NB: 0 for heartrate
//        playSoundFX(0);
        mPlayer.playSoundFX(mHeartRateBPM);

        // Resume timer
        startTiming();
    }

    /**
     * Set up assessment for saving to database
     */
    void setupAssessmentRecord() {
        if (mAssessmentList != null) {
            mCurrentAssessment.number = mAssessmentList.size() + 1;
            mCurrentAssessment.quiz = mQuiz;
            mCurrentAssessment.type = getAssessmentType();
            mCurrentAssessment.correctBPM = mHeartRateBPM;
            mCurrentAssessment.correctCategory = mHeartRateCategory;
            mCurrentAssessment.timeElapsed = mElapsedTime;

            // Add assessment to session list
            mAssessmentList.add(mCurrentAssessment);
        }
    }

    /**
     * Saves response submitted by user and optionally displays results of the
     * assessment
     *
     * @param assessmentType the type of assessment being taken
     * @param response       the response provided
     */
    void gradeAssessment(int assessmentType, String response) {

        boolean instant = mQuizMode.equals("training");

        switch (assessmentType) {
            case HR_ACTUAL_BPM:
                int bpm = Integer.parseInt(response);
                mCurrentAssessment.answerBPM = bpm;

                if (instant) {
                    if (bpm == mHeartRateBPM) {
                        Toast.makeText(getActivity(), "Correct!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(
                                getActivity(),
                                "Wrong! The heart rate was "
                                        + String.valueOf(mHeartRateBPM)
                                        + " BPM"
                                        + " ("
                                        + Utility.round(Utility.calculateError(
                                        mHeartRateBPM, bpm), 0) + " %)",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case HR_BPM_CATEGORY:
                mCurrentAssessment.answerCategory = response;

                if (instant) {
                    if (response.equals(mHeartRateCategory)) {
                        Toast.makeText(getActivity(), "Correct!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(
                                getActivity(),
                                "Wrong! The heart rate was"
                                        + String.valueOf(mHeartRateBPM)
                                        + " BPM ("
                                        + String.valueOf(Assessment
                                        .toCategory(mHeartRateCategory)) + ")",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

	/* QUIZ */

    /**
     * Starts a quiz
     */
    void startQuiz() {
        // mQuizInSession = true;

        // Create quiz
        createQuizRecord(); // TODO: use this in place of mQuizSession

        // TODO: Moving this to beginning of assessment instead
//        if (mTimerMode == Quiz.TIMER_MODE_CLOCK)
//            // Start timer clock if needed
//            startClock();
//
//        if (mTimerMode == Quiz.TIMER_MODE_FLASHING_TIMER)
//            // Start flasher
//            startFlashingTimer(); // TODO: kill on endquiz?
    }

    /**
     * Ends current quiz in session
     *
     * @param successful specifies whether quiz was completed successfully or not
     */
    void endQuiz(boolean successful) {
        // mQuizInSession = false;

        // Stop flashing timer if started
        if (mFlashingTimer != null)
            mFlashingTimer.stop();

        // Stop clock
        stopClock();

        // If quiz ended successfully, save and display results
        if (successful) {
            long quizId = saveQuiz();

            // Show results screen
            Intent intent = new Intent(getActivity(), QuizDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putLong("quizId", quizId);
            intent.putExtras(bundle);
            getActivity().startActivity(intent);
            getActivity().finish();
        }

        // Clear mQuiz
        mQuiz = null;
        // Clear assessment list
        mAssessmentList = null;
    }

    /**
     * Saves quiz results to database
     */
    long saveQuiz() {
        if (mQuiz != null) {
            // Save quiz
            mQuiz.save();

            // Save assessments
            ActiveAndroid.beginTransaction();
            try {
                for (int i = 0; i < mAssessmentList.size(); i++) {
                    mAssessmentList.get(i).save();
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }
            return mQuiz.getId(); // return id of quiz saved
        }
        return (long) 0;
    }

    /**
     * Creates a new quiz object
     */

    void createQuizRecord() {
        // Set up quiz
        mQuiz = new Quiz();

        mQuiz.user = User.findById(mSessionManager.getLoggedInUserId());
        mQuiz.mode = mTimerMode;
        mQuiz.assessmentType = getAssessmentType();
        mQuiz.setCreatedTimestamp(new Date().toString());

        // TODO: Inefficient!!
        // TODO: Move to model
        int quizNumber = Quiz.findAll(
                Long.toString(mSessionManager.getLoggedInUserId()), mTimerMode,
                getAssessmentType()).size() + 1;

        String prefix = null;
        if (mTimerMode == Quiz.TIMER_MODE_CLOCK)
            prefix = "C";

        if (mTimerMode == Quiz.TIMER_MODE_FLASHING_TIMER)
            prefix = "F";

        if (mTimerMode == Quiz.TIMER_MODE_RANDOM)
            prefix = "R";

        mQuiz.number = prefix + quizNumber;

        // Initialize assessment array
        mAssessmentList = new ArrayList<Assessment>();
    }

	/* TIMERS */

    /**
     * Starts flashing timer which flashes screen every 6 seconds
     */
    void startFlashingTimer() {
        mFlashingTimer = new FlashingTimer(getActivity(), 6,
                mFrameLayout, (mQuiz != null), true);
        mFlashingTimer.start();
    }

    void stopFlashingTimer() {
        if (mFlashingTimer != null)
            mFlashingTimer.stop();
    }

    /**
     * Starts chronometer from random time between 10 and 30 seconds
     */
    void startClock() {
        int startTime = 0;
        // Start from a random time between 10s and 30s
        startTime = new Random().nextInt((30 - 10) + 1) + 10;
        startChronometer(startTime);
    }

    /**
     * Stops clock
     */
    void stopClock() {
        // Stop chronometer
        stopChronometer();
    }

    void useClockTimer() {
        // Show clock and Resuscitaire image
        mClockLayoutLeftColumn.setVisibility(View.VISIBLE);
        mChronometer.setVisibility(View.VISIBLE);
        // Start clock
        startClock();
    }

    void useFlashingTimer() {
        // Hide clock and resuscitaire image
        mChronometer.setVisibility(View.INVISIBLE);
        mClockLayoutLeftColumn.setVisibility(View.GONE);
        // Start timer
        startFlashingTimer(); // TODO: kill on endquiz?
    }

    /**
     * Determines what timer mode to use and sets this up
     */
    void setAssessmentTimer(Assessment assessment) {
        // TODO: set assessment's timer mode

        // TODO: Stop all running timers esp flashing
        stopFlashingTimer();

        assessment.timerMode = mTimerMode;

        if (mTimerMode == Quiz.TIMER_MODE_CLOCK)
            useClockTimer();

        if (mTimerMode == Quiz.TIMER_MODE_FLASHING_TIMER)
            useFlashingTimer();

        if (mTimerMode == Quiz.TIMER_MODE_RANDOM) {
            String timerMode;
            int value = new Random().nextInt(2);
            int limit = mAssessmentCount / 2;
            if (value == 0) {
                // clock
                if (mClockAssessmentsCount < limit) {
                    mClockAssessmentsCount++;
                    timerMode = Quiz.TIMER_MODE_CLOCK;
                    useClockTimer();
                } else {
                    mFTimerAssessmentsCount++;
                    timerMode = Quiz.TIMER_MODE_FLASHING_TIMER;
                    useFlashingTimer();
                }
            } else {
                // flashing timer
                if (mFTimerAssessmentsCount < limit) {
                    mFTimerAssessmentsCount++;
                    timerMode = Quiz.TIMER_MODE_FLASHING_TIMER;
                    useFlashingTimer();
                } else {
                    mClockAssessmentsCount++;
                    timerMode = Quiz.TIMER_MODE_CLOCK;
                    useClockTimer();
                }
            }
            // Set actual timer mode
            assessment.timerMode = timerMode;
        }


//        String a = "Total: " + Integer.toString(mAssessmentCount / 2) +
//                " Clock: " + mClockAssessmentsCount +
//                " Flash: " + mFTimerAssessmentsCount;
//
//        Toast.makeText(getActivity(), a, Toast.LENGTH_SHORT).show();
    }

//	/* SOUND */
//
//    /**
//     * Plays heart beat sound effect at a specified rate. Uses a 50bpm snippet
//     * whose tempo is increased to achieve the effect of changing heart rate
//     *
//     * @param heartRate the heart rate to play in BPM.
//     */
//    private void playSoundFX(int heartRate) {
//        // Show pause icon
//        mBtnAssessment.setImageResource(R.drawable.btn_pause);
//
//        int resource = R.raw.bpm70;
//
//        switch (heartRate) {
//            case 50:
//                resource = R.raw.bpm50;
//                break;
//            case 55:
//                resource = R.raw.bpm55;
//                break;
//            case 60:
//                resource = R.raw.bpm60;
//                break;
//            case 65:
//                resource = R.raw.bpm65;
//                break;
//            case 70:
//                resource = R.raw.bpm70;
//                break;
//            case 75:
//                resource = R.raw.bpm75;
//                break;
//            case 80:
//                resource = R.raw.bpm80;
//                break;
//            case 85:
//                resource = R.raw.bpm85;
//                break;
//            case 90:
//                resource = R.raw.bpm90;
//                break;
//            case 95:
//                resource = R.raw.bpm95;
//                break;
//            case 100:
//                resource = R.raw.bpm100;
//                break;
//            case 105:
//                resource = R.raw.bpm105;
//                break;
//            case 110:
//                resource = R.raw.bpm110;
//                break;
//            case 115:
//                resource = R.raw.bpm115;
//                break;
//            case 120:
//                resource = R.raw.bpm120;
//                break;
//            case 125:
//                resource = R.raw.bpm125;
//                break;
//            case 130:
//                resource = R.raw.bpm130;
//                break;
//            case 135:
//                resource = R.raw.bpm135;
//                break;
//            case 140:
//                resource = R.raw.bpm140;
//                break;
//            case 145:
//                resource = R.raw.bpm145;
//                break;
//            case 150:
//                resource = R.raw.bpm150;
//                break;
//        }
//
//        mSoundPlayer = MediaPlayer.create(getActivity(), resource);
//        if (!mSoundPlayer.isLooping())
//            mSoundPlayer.setLooping(true);
//        mSoundPlayer.start();
//    }
//
//    /**
//     * Pauses sound. Does not delete current assessment session data
//     */
//    private void pauseSoundFX() {
//        // Show play icon
//        mBtnAssessment.setImageResource(R.drawable.btn_play);
//        // Pause player
//        if (mSoundPlayer != null)
//            mSoundPlayer.pause();
//    }
//
//    /**
//     * Stops sound
//     */
//    private void stopSoundFX() {
//        // Show play icon
//        mBtnAssessment.setImageResource(R.drawable.btn_play);
//        // Stop player
//        if (mSoundPlayer != null)
//            mSoundPlayer.release();
//    }

	/* ASSESSMENT TIMING */

    /**
     * Starts timing an assessment
     */
    private void startTiming() {
        // set start time to current time
        // mStartTime = System.currentTimeMillis();
        mStartTime = System.nanoTime();
    }

    /**
     * Stops timing and determines elapsed time since start
     */
    private void stopTiming() {
        // Get stop time
        // long stopTime = System.currentTimeMillis();
        long stopTime = System.nanoTime();

        // Update elapsed time
        mElapsedTime += stopTime - mStartTime;
    }

    /**
     * Resets timer
     */
    private void resetTiming() {
        mElapsedTime = 0;
    }

	/* TIMER CLOCK */

    /**
     * Starts chronometer at time supplied
     *
     * @param startTime Time to start counting from in seconds
     */
    void startChronometer(long startTime) {
        if (mChronometer != null) {
            // Set starting time
            mChronometer.setBase(SystemClock.elapsedRealtime()
                    - (startTime * 1000));
            // Start counting
            mChronometer.start();
        }
    }

    /**
     * Stops chronometer
     */
    void stopChronometer() {
        if (mChronometer != null) {
            // Stop counting
            mChronometer.stop();

            // Set chronometer's display to 00:00
            mChronometer.setBase(SystemClock.elapsedRealtime());
        }
    }

	/* DIALOGS */

    /**
     * Display dialog to quit quiz
     */
    private void showQuitQuizDialog() {
        if (mAssessmentInSession)
            pauseAssessment();

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Quit Quiz?")
                .setMessage("You'll lose all progress!")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // resume assessment if it was paused because of this
                        // dialog
                        if (mAssessmentInSession)
                            resumeAssessment();
                    }
                })
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO: more testing
                                // Stop assessment
                                endAssessment(false);
                                // End quiz
                                getActivity().finish();
                            }
                        }).show();
    }

    /**
     * Displays dialog to reset quiz
     */
    private void showRestartQuizDialog() {
        if (mAssessmentInSession)
            pauseAssessment();

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Restart Quiz?")
                .setMessage("You'll lose all progress!")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // resume assessment if it was paused because of this
                        // dialog
                        if (mAssessmentInSession)
                            resumeAssessment();
                    }
                })
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO: more testing
                                // End current assessment
                                endAssessment(false);

                                // End current quiz
                                endQuiz(false);

                                // Get new settings for quiz
                                showQuizSetupDialog();
                            }
                        }).show();
    }

    /**
     * Displays either one of two dialog types, based on assessment type
     * specified during quiz setup, to receive input for heart rate
     */
    private void showInputHeartRateDialog(int dialogType) {
        // Pause assessment
        pauseAssessment();

        // Display user's preferred dialog
        DialogFragment fragment = null;
        switch (dialogType) {
            case HR_ACTUAL_BPM:
                fragment = InputHeartRateBPMDialog.newInstance();
                break;
            case HR_BPM_CATEGORY:
                fragment = InputHeartRateCategoryDialog.newInstance();
                break;
        }
        fragment.setTargetFragment(this, dialogType);
        fragment.show(getActivity().getSupportFragmentManager(), "input_bpm");
    }

    /**
     * Shows a dialog that allows user to set up the quiz
     */
    private void showQuizSetupDialog() {
        DialogFragment fragment = QuizSetupDialog.newInstance(mTimerMode);
        // Set callback to this fragment from the dialogFragment
        fragment.setTargetFragment(this, QUIZ_SETUP_DIALOG);
        fragment.show(getActivity().getSupportFragmentManager(), "quiz_setup");
    }

	/* MISC */

    /**
     * Gets type of assessment for current quiz
     *
     * @return Returns assessment value type
     */
    String getAssessmentType() {
        // Set type of assessment
        switch (mAssessmentType) {
            case HR_ACTUAL_BPM:
                return Quiz.ASSESSMENT_ACTUAL;
            case HR_BPM_CATEGORY:
                return Quiz.ASSESSMENT_CATEGORICAL;
            default:
                return null;
        }
    }

    /**
     * Generates a random heart rate between 50 and 150 BPM
     */
    private int generateRandomHeartRate() {
        Random random = new Random(System.currentTimeMillis());
        int value = (random.nextInt(30 - 10) + 10) * 5;
        return value;
    }

    /**
     * Returns one of three heart rate categories based on heart rate provided
     *
     * @param heartRate Heart rate to determine category for
     * @return Integer representing category heart rate is in
     */
    private String getHeartRateCategory(int heartRate) {
        if (heartRate < 60) {
            return HR_CATEGORY_BELOW_60;
        } else if ((heartRate >= 60) && (heartRate <= 100)) {
            return HR_CATEGORY_60_100;
        } else {
            return HR_CATEGORY_OVER_100;
        }
    }

	/* INTERFACE IMPLEMENTATIONS */

    /**
     * Handles positive response from "Enter Heart Rate BPM" popup
     *
     * @param bpm Heart rate entered by user in BPM
     */
    @Override
    public void onInputHRBpmDialogOK(String bpm) {
        // Grade response
        gradeAssessment(HR_ACTUAL_BPM, bpm);

        // Proceed with quiz
        proceedToNextAssessment();
    }

    /**
     * Handles positive response from "Enter Heart Rate Category" popup
     *
     * @param categoryId Id of radio button selected in dialog
     */
    @Override
    public void onInputHRCategoryDialogOK(int categoryId) {
        String category = null;
        switch (categoryId) {
            case R.id.rbtn_below_60:
                category = HR_CATEGORY_BELOW_60;
                break;
            case R.id.rbtn_60_to_100:
                category = HR_CATEGORY_60_100;
                break;
            case R.id.rbtn_over_100:
                category = HR_CATEGORY_OVER_100;
                break;
        }
        // Grade response
        gradeAssessment(HR_BPM_CATEGORY, category);

        // Proceed with quiz
        proceedToNextAssessment();
    }

    /**
     * Handles negative response from both input heart rate dialogs
     */
    @Override
    public void onInputHRDialogCancel() {
        // Resume current assessment
        resumeAssessment();
    }

    @Override
    public void onQuizSetupDialogClick(int assessmentTypeId,
                                       int assessmentCountId) {

        // Set type of assessment
        switch (assessmentTypeId) {
            case R.id.rbtn_bpm:
                mAssessmentType = HR_ACTUAL_BPM;
                break;
            case R.id.rbtn_category:
                mAssessmentType = HR_BPM_CATEGORY;
                break;
        }

        // Set number of assessments
        switch (assessmentCountId) {
            case R.id.rbtn_1:
                mAssessmentCount = 1;
                break;
            case R.id.rbtn_5:
                mAssessmentCount = 5;
                break;
            case R.id.rbtn_8:
                mAssessmentCount = 8;
                break;
            case R.id.rbtn_10:
                mAssessmentCount = 10;
                break;
            case R.id.rbtn_12:
                mAssessmentCount = 12;
                break;
            case R.id.rbtn_15:
                mAssessmentCount = 15;
                break;
            case R.id.rbtn_16:
                mAssessmentCount = 16;
                break;
            case R.id.rbtn_20:
                mAssessmentCount = 20;
                break;
            default:
                mAssessmentCount = 1;
                break;
        }
    }

}
