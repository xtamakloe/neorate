package com.ismat.neorate.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.ismat.neorate.activity.app.SessionActivity;
import com.ismat.neorate.model.User;

import java.util.HashMap;

/**
 * Handles user session management
 *
 */
public class SessionManager {

	SharedPreferences mPreferences;
	SharedPreferences.Editor mEditor;
	Context mContext;
	Activity mActivity;

	int PRIVATE_MODE = 0;

	public static final String PREF_NAME = "heartbeat_prefs";
	public static final String IS_LOGGED_IN = "IsLoggedIn";
	public static final String KEY_NAME = "name";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_TOKEN = "";
	public static final String KEY_USER_ID = "user_id";

	public SessionManager(Activity activity) {
		this.mActivity = activity;
		this.mContext = activity.getApplicationContext();

		mPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		// mEditor = mPreferences.edit();
	}

	// Create login session
	public void createLoginSession(String name, String username, String token,
			long userId) {
		mEditor = mPreferences.edit();
		mEditor.putString(KEY_NAME, name);
		mEditor.putString(KEY_USERNAME, username);
		mEditor.putString(KEY_TOKEN, token);
		mEditor.putString(SessionManager.KEY_USER_ID, Long.toString(userId));
		mEditor.putBoolean(IS_LOGGED_IN, true);
		mEditor.commit();
	}

	// Check user login status
	public void checkLogin() {
		// Redirect to login page if not logged in
		if (!this.isLoggedIn()) {
			Intent i = new Intent(mContext, SessionActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // close all activities
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // add new flag to start
														// new Activity
			mContext.startActivity(i);
			mActivity.finish();
		}
	}

	// Clear session details
	public void logoutUser() {
		mEditor = mPreferences.edit();
		// clear user data from SharedPrefs
		mEditor.remove(SessionManager.KEY_NAME);
		mEditor.remove(SessionManager.KEY_USERNAME);
		mEditor.remove(SessionManager.KEY_TOKEN);
		mEditor.remove(SessionManager.KEY_USER_ID);
		mEditor.putBoolean(IS_LOGGED_IN, false);
		mEditor.commit();

		// After logout redirect user to Login Activity
		Intent i = new Intent(mContext, SessionActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mContext.startActivity(i);
		mActivity.finish();
	}

	// Quick check for login
	public boolean isLoggedIn() {
		return mPreferences.getBoolean(IS_LOGGED_IN, false);
	}

	// Get stored session data
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		user.put(KEY_NAME, mPreferences.getString(KEY_NAME, null));
		user.put(KEY_USERNAME, mPreferences.getString(KEY_USERNAME, null));
		user.put(KEY_USER_ID, mPreferences.getString(KEY_USER_ID, null));
		return user;
	}

	// Get user id of logged in user
	public long getLoggedInUserId() {
		return Long.parseLong(mPreferences.getString(KEY_USER_ID, null));
	}

	public boolean isAdminSession() {
		return User.findById(getLoggedInUserId()).isAdmin();
	}
}