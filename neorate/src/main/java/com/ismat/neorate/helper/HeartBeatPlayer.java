package com.ismat.neorate.helper;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.widget.ImageButton;

import com.ismat.neorate.R;

/**
 * Created by kojo on 25/09/2014.
 */
public class HeartBeatPlayer {
    private ImageButton mPlayButton;
    private MediaPlayer mSoundPlayer;
    private Context mContext;

    public HeartBeatPlayer(Context context, ImageButton playButton) {
        this.mPlayButton = playButton;
        this.mContext = context;
    }

    /**
     * Plays heart beat sound effect at a specified rate.
     *
     * @param heartRate the heart rate to play in BPM.
     */
    public void playSoundFX(int heartRate) {

        if (mSoundPlayer != null)
            mSoundPlayer.release();

        // Show pause icon
        if (mPlayButton != null)
            mPlayButton.setImageResource(R.drawable.btn_pause);

        int resource = 0;

        switch (heartRate) {
            case 50:
                resource = R.raw.bpm50;
                break;
            case 55:
                resource = R.raw.bpm55;
                break;
            case 60:
                resource = R.raw.bpm60;
                break;
            case 65:
                resource = R.raw.bpm65;
                break;
            case 70:
                resource = R.raw.bpm70;
                break;
            case 75:
                resource = R.raw.bpm75;
                break;
            case 80:
                resource = R.raw.bpm80;
                break;
            case 85:
                resource = R.raw.bpm85;
                break;
            case 90:
                resource = R.raw.bpm90;
                break;
            case 95:
                resource = R.raw.bpm95;
                break;
            case 100:
                resource = R.raw.bpm100;
                break;
            case 105:
                resource = R.raw.bpm105;
                break;
            case 110:
                resource = R.raw.bpm110;
                break;
            case 115:
                resource = R.raw.bpm115;
                break;
            case 120:
                resource = R.raw.bpm120;
                break;
            case 125:
                resource = R.raw.bpm125;
                break;
            case 130:
                resource = R.raw.bpm130;
                break;
            case 135:
                resource = R.raw.bpm135;
                break;
            case 140:
                resource = R.raw.bpm140;
                break;
            case 145:
                resource = R.raw.bpm145;
                break;
            case 150:
                resource = R.raw.bpm150;
                break;
        }

        mSoundPlayer = MediaPlayer.create(mContext, resource);
        if (!mSoundPlayer.isLooping())
            mSoundPlayer.setLooping(true);
        mSoundPlayer.start();
    }

    /**
     * Pauses sound. Does not delete current assessment session data
     */
    public void pauseSoundFX() {
        // Show play icon
        if (mPlayButton != null)
            mPlayButton.setImageResource(R.drawable.btn_play);

        // Pause player
        if (mSoundPlayer != null)
            mSoundPlayer.pause();
    }

    /**
     * Stops sound
     */
    public void stopSoundFX() {
        // Show play icon
        if (mPlayButton != null)
            mPlayButton.setImageResource(R.drawable.btn_play);

        // Stop player
        if (mSoundPlayer != null)
            mSoundPlayer.release();
    }
}
