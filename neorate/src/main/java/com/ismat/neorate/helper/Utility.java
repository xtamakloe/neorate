package com.ismat.neorate.helper;

import java.math.BigDecimal;

/**
 * Holds utility methods used across app
 */
public class Utility {

	public static float calculateError(int correct, int submitted) {
		int difference = Math.abs(submitted - correct);
		// float accuracy = 100 - ((difference / (float) correct) * 100);
		return (difference / (float) correct) * 100;
	}

	public static String round(Float figure, int scale) {
		BigDecimal rounded = new BigDecimal(Float.toString(figure));
		rounded = rounded.setScale(scale, BigDecimal.ROUND_HALF_UP);
		return rounded.toString();
	}

	public static String formatTime(long timeInNanos) {
		Float seconds = timeInNanos / 1000000000.0f;
		return round(seconds, 1) + "s";

		// long ts = TimeUnit.NANOSECONDS.toSeconds(timeInNanos);
		// long tm = TimeUnit.NANOSECONDS.toMinutes(timeInNanos);
		// StringBuilder formattedTime = new StringBuilder();
		// // Add 'm' for minutes
		// if (tm != 0)
		// formattedTime.append(Long.toString(tm) + "m ");
		// // Add 's' for minutes
		// if (ts != 0)
		// formattedTime.append(Long.toString(ts) + "s");
		// return formattedTime.toString();
	}
}
