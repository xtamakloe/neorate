package com.ismat.neorate.helper;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import java.util.Random;

/**
 * Visual flashing timer used in quizzes or on its own
 */
public class FlashingTimer {

    private Activity mActivity;
    private int mInterval;
    private View mLayout;
    private boolean mCondition;
    private boolean mLateStart;
    private Thread mTimerThread;

    public FlashingTimer(Activity activity, int flashingInterval, View layout,
                         boolean condition, boolean lateStart) {
        this.mActivity = activity;
        this.mInterval = flashingInterval;
        this.mLayout = layout;
        this.mCondition = condition;
        this.mLateStart = lateStart;
    }

    class TimerThread implements Runnable {
        public void run() {
            final int interval = (mInterval * 1000) - 200;

            if (mLateStart) {
                int onStartWaitInterval = 1000 * (new Random().nextInt((5 - 1) + 1) + 1);
                startFlashing(onStartWaitInterval);
            }
            while (mCondition) {
                startFlashing(interval);
            }
        }
    }

    public void start() {
        // Number of seconds between changing colors, makes allowance for
        // time second color is displayed
        mTimerThread = new Thread(new TimerThread());
        mTimerThread.start();
    }


    void startFlashing(int interval) {
        // Wait for interval
        if (mCondition) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                mCondition = false;
            }
        } else {
            // end thread
        }

        // Show white screen for 0.2s
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCondition) {
                        if (mLayout != null)
                            mLayout.setBackgroundColor(Color.WHITE);
                    } else {
                        // end thread
                    }
                }
            });
            if (mCondition) {
                try {
                    Thread.sleep(200); // wait for 0.2s
                } catch (InterruptedException e) {
                    mCondition = false;
                }
            } else {
                // end thread
            }
        }

        // Show black screen
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCondition) {
                        if (mLayout != null)
                            mLayout.setBackgroundColor(Color.BLACK);
                    } else {
                        // end thread here
                    }
                }
            });
        }
    }

    public void stop() {
        mCondition = false;
    }
}
