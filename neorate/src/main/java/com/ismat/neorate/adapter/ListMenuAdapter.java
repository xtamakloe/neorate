package com.ismat.neorate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ismat.neorate.R;

/**
 * Custom adapter used for displaying menus in a list
 * 
 */
public class ListMenuAdapter extends ArrayAdapter<String> {

	String[] mDescriptions;
	Integer[] mIcons;

	public ListMenuAdapter(Context context, String[] titles,
			String[] descriptions, Integer[] resources) {
		super(context, R.layout.row_layout, R.id.listlabel, titles);
		this.mDescriptions = descriptions;
		this.mIcons = resources;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);

		ImageView icon = (ImageView) row.findViewById(R.id.icon);

		icon.setImageResource(mIcons[position]);

		((TextView) row.findViewById(R.id.secondLine))
				.setText(mDescriptions[position]);
		return (row);
	}
}
