package com.ismat.neorate.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.model.Quiz;

import java.util.List;

/**
 * Custom adapter for displaying a list of quizzes
 * 
 *
 */
public class QuizListsAdapter extends BaseAdapter {

	private Activity mActivity;
	private List<Quiz> mQuizList;
	private Quiz mQuiz;

	private static LayoutInflater mInflater = null;

	public QuizListsAdapter(Activity activity, List<Quiz> quizzes) {
		this.mActivity = activity;
		this.mQuizList = quizzes;
		mInflater = (LayoutInflater) this.mActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		if (mQuizList.size() <= 0)
			return 1;

		return mQuizList.size();
	}

	@Override
	public Object getItem(int position) {
		return mQuizList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		
		if (convertView == null) {
			view = mInflater.inflate(R.layout.row_item_quizzes_list, null);

			holder = new ViewHolder();
			holder.name = (TextView) view.findViewById(R.id.tv_quiz_name);
            holder.date = (TextView) view.findViewById(R.id.tv_quiz_date);

			view.setTag(holder);
		} else
			holder = (ViewHolder) view.getTag();

		if (mQuizList.size() <= 0) {
			holder.name.setText("");
            holder.date.setText("");
		} else {
			mQuiz = null; 
			mQuiz = mQuizList.get(position);

			holder.name.setText(mQuiz.getTitle());
            holder.date.setText(mQuiz.getCreatedTimestamp());
		}

		return view;
	}

	public static class ViewHolder {
		public TextView name;
        public TextView date;
	}

}
