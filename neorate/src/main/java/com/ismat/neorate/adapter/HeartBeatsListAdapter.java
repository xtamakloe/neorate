package com.ismat.neorate.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ismat.neorate.R;

import java.util.List;

/**
 * Created by kojo on 25/09/2014.
 */
public class HeartBeatsListAdapter extends BaseAdapter {

    private Activity mActivity;
    private List<String> mHeartRates;
    private static LayoutInflater mInflater = null;

    public HeartBeatsListAdapter(Activity activity, List<String> heartRates) {
        this.mActivity = activity;
        this.mHeartRates = heartRates;
        mInflater = (LayoutInflater) this.mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (mHeartRates.size() <= 0)
            return 1;
        return mHeartRates.size();
    }

    @Override
    public Object getItem(int position) {
        return mHeartRates.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_item_heart_beats_list, null);

            holder = new ViewHolder();
            holder.label = (TextView) view.findViewById(R.id.lbl_heartbeat_label);

            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        if (mHeartRates.size() <= 0) {
            holder.label.setText("");
        } else {
            holder.label.setText(mHeartRates.get(position));
        }

        return view;
    }

    public static class ViewHolder {
        public TextView label;
    }
}
