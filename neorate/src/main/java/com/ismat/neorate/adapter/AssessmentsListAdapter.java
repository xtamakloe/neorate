package com.ismat.neorate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.model.Assessment;

import java.util.List;

/**
 * Custom adapter for displaying a list of assessments
 * 
 * @author Christian Tamakloe
 * 
 */
public class AssessmentsListAdapter extends BaseAdapter {

	private static LayoutInflater mInflater = null;
	private List<Assessment> mAssessments;
	private Context mContext;

    public AssessmentsListAdapter(List<Assessment> mAssessments, Context context) {
		super();
		this.mAssessments = mAssessments;
		this.mContext = context;
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		if (mAssessments.size() <= 0)
			return -1;
		return mAssessments.size();
	}

	@Override
	public Object getItem(int position) {
		return mAssessments.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Assessment assessment = mAssessments.get(position);
        TextView number, duration, correctAnswer, usersAnswer, accuracy;
        LinearLayout quizNumberBg;
        String error = null;

        if (convertView == null)
            convertView = mInflater.inflate(R.layout.row_item_assessments_list,
                    null);

        quizNumberBg = (LinearLayout) convertView
                .findViewById(R.id.ll_quiz_number);
        number = (TextView) convertView
                .findViewById(R.id.tv_assessment_number);
        duration = (TextView) convertView
                .findViewById(R.id.tv_assessment_duration);
        correctAnswer = (TextView) convertView
                .findViewById(R.id.tv_assessment_correct_answer);
        usersAnswer = (TextView) convertView
                .findViewById(R.id.tv_assessment_users_answer);
        accuracy = (TextView) convertView
                .findViewById(R.id.tv_assessment_accuracy);

        if (assessment.isSuccessful()) {
            convertView
                    .setBackgroundResource(R.drawable.assessments_list_bg_success);
        } else {
            convertView
                    .setBackgroundResource(R.drawable.assessments_list_bg_fail);
        }

        if (assessment.quiz.isRandom()) {
            if (assessment.isClockAssessment()) {
                quizNumberBg.setBackgroundResource(
                        R.drawable.assessments_list_quiz_number_clock);
            } else if (assessment.isTimerAssessment()) {
                quizNumberBg.setBackgroundResource(
                        R.drawable.assessments_list_quiz_number_timer);
            }
        }

        if (mAssessments.size() <= 0) {
            number.setText("");
            duration.setText("");
            correctAnswer.setText("");
            usersAnswer.setText("");
            accuracy.setText("");
        } else {
            number.setText(assessment.getNumber());
            duration.setText(assessment.getDuration());
            correctAnswer.setText(assessment.getCorrectAnswer());
            usersAnswer.setText(assessment.getUsersAnswer());
            if (assessment.isActual()) {
                error = "(" + assessment.getError() + "% off)";
            }
            accuracy.setText(error);
        }
        return convertView;
    }

//	public static class ViewHolder {
//		TextView number;
//		TextView duration;
//		TextView correctAnswer;
//		TextView usersAnswer;
//		TextView accuracy;
//	}

//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		ViewHolder holder;
//		Assessment assessment = mAssessments.get(position);
//
//		if (convertView == null) {
//			convertView = mInflater.inflate(R.layout.row_item_assessments_list,
//					null);
//
//			holder = new ViewHolder();
//			holder.number = (TextView) convertView
//					.findViewById(R.id.tv_assessment_number);
//			holder.duration = (TextView) convertView
//					.findViewById(R.id.tv_assessment_duration);
//			holder.correctAnswer = (TextView) convertView
//					.findViewById(R.id.tv_assessment_correct_answer);
//			holder.usersAnswer = (TextView) convertView
//					.findViewById(R.id.tv_assessment_users_answer);
//			holder.accuracy = (TextView) convertView
//					.findViewById(R.id.tv_assessment_accuracy);
//
//			if (assessment.isSuccessful()) {
//				convertView
//						.setBackgroundResource(R.drawable.assessments_list_bg_success);
//			} else {
//				convertView
//						.setBackgroundResource(R.drawable.assessments_list_bg_fail);
//			}
//
//            LinearLayout quizNumberBg = (LinearLayout) convertView
//                    .findViewById(R.id.ll_quiz_number);
//            if (assessment.quiz.isRandom()) {
//                if (assessment.isClockAssessment()) {
//                    quizNumberBg.setBackgroundResource(
//                            R.drawable.assessments_list_quiz_number_timer);
//                } else if (assessment.isTimerAssessment()) {
//                    quizNumberBg.setBackgroundResource(
//                            R.drawable.assessments_list_quiz_number_clock);
//                }
//            }
//
//			convertView.setTag(holder);
//		} else
//			holder = (ViewHolder) convertView.getTag();
//
//		if (mAssessments.size() <= 0) {
//			holder.number.setText("");
//			holder.duration.setText("");
//			holder.correctAnswer.setText("");
//			holder.usersAnswer.setText("");
//			holder.accuracy.setText("");
//		} else {
//			holder.number.setText(assessment.getNumber());
//			holder.duration.setText(assessment.getDuration());
//			holder.correctAnswer.setText(assessment.getCorrectAnswer());
//			holder.usersAnswer.setText(assessment.getUsersAnswer());
//
//			String error = null;
//			if (assessment.isActual()) {
//				error = "(" + assessment.getError() + "% off)";
//			}
//			holder.accuracy.setText(error);
//		}
//
//		return convertView;
//	}
}