package com.ismat.neorate.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import com.ismat.neorate.R;
import com.ismat.neorate.model.Quiz;

/**
 * {@link DialogFragment} that allows users to select parameters for creating a
 * chart
 * 
 */
public class ChartSetupDialog extends DialogFragment {

	ChartSetupDialogListener mListener;

	public interface ChartSetupDialogListener {
		public void onChartSetupDialogClick(int quizAssessmentTypeId,
				String valuesType);
	}

	public static ChartSetupDialog newInstance() {
		return new ChartSetupDialog();
	}

	public ChartSetupDialog() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		try {
			mListener = (ChartSetupDialogListener) getTargetFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException("Calling fragment must implement "
					+ "ChartSetupDialogListener interface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);

		LayoutInflater inflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View view = inflater.inflate(
				R.layout.dialog_fragment_chart_setup, null);

		final RadioGroup rgQuizCount, rgValueType;
		rgQuizCount = (RadioGroup) view.findViewById(R.id.rg_quiz_count);
		rgValueType = (RadioGroup) view.findViewById(R.id.rg_assessment_values);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Chart Options")
				.setView(view)
				.setPositiveButton("DONE",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Get user choices

								int quizCount = 0;
								String valuesType = null;

								// Get value type
								switch (rgValueType.getCheckedRadioButtonId()) {
								case R.id.rbtn_actual:
									valuesType = Quiz.ASSESSMENT_ACTUAL;
									break;
								case R.id.rbtn_categorical:
									valuesType = Quiz.ASSESSMENT_CATEGORICAL;
									break;
								}

								// Get number of quizzes
								switch (rgQuizCount.getCheckedRadioButtonId()) {
								case R.id.rbtn_5_quizzes:
									quizCount = 5;
									break;
								case R.id.rbtn_25_quizzes:
									quizCount = 25;
									break;
								case R.id.rbtn_50_quizzes:
									quizCount = 50;
									break;
								case R.id.rbtn_all_quizzes:
									quizCount = -1; // -1 for all quizzes
									break;
								default:
									quizCount = -1;
									break;
								}

								if (mListener != null)
									mListener.onChartSetupDialogClick(
											quizCount, valuesType);

							}
						});
		return builder.create();
	}
}