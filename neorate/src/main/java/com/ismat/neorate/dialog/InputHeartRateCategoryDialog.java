package com.ismat.neorate.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ismat.neorate.R;

/**
 * {@link DialogFragment} that allows users to select the category of the heart
 * rate they listen to during an assessment
 * 
 */
public class InputHeartRateCategoryDialog extends DialogFragment {

	InputHeartRateCategoryDialogListener mListener;

	public interface InputHeartRateCategoryDialogListener {
		public void onInputHRCategoryDialogOK(int categoryId);

		public void onInputHRDialogCancel();
	}

	/**
	 * 
	 * Factory method to create new instance of this fragment using the provided
	 * parameters.
	 * 
	 * @return A new instance of fragment
	 *         {@link InputHeartRateCategoryDialogListener}.
	 */
	public static InputHeartRateCategoryDialog newInstance() {
		return new InputHeartRateCategoryDialog();
	}

	public InputHeartRateCategoryDialog() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		try {
			mListener = (InputHeartRateCategoryDialogListener) getTargetFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException("Calling fragment must implement "
					+ "InputHeartRateCategoryDialogListener interface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View view = inflater.inflate(
				R.layout.dialog_fragment_enter_heart_rate_category, null);
		final AlertDialog.Builder builder = new AlertDialog.Builder(
				getActivity());

		// Radio group containing categories
		final RadioGroup rgCat = (RadioGroup) view
				.findViewById(R.id.rg_hr_category);

		builder.setView(view)
				.setTitle("Select Heart Rate Category")
				.setNegativeButton("CANCEL",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (mListener != null)
									mListener.onInputHRDialogCancel();
							}
						})
				.setPositiveButton("PROCEED",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								int selectedId = rgCat
										.getCheckedRadioButtonId();

								if (selectedId != -1) {
									// Return id of category selected
									if (mListener != null)
										mListener
												.onInputHRCategoryDialogOK(rgCat
														.getCheckedRadioButtonId());
								} else {
									Toast.makeText(
											getActivity(),
											"Please select a cateogry for the heart rate",
											Toast.LENGTH_SHORT).show();
								}

							}
						});

		return builder.create();
	}
}
