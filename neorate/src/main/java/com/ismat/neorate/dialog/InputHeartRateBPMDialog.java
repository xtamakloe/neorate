package com.ismat.neorate.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ismat.neorate.R;

/**
 * A simple {@link DialogFragment} that allows users to input the BPM of the
 * heart rate they listen to. Provides a custom keypad.
 * 
 */
public class InputHeartRateBPMDialog extends DialogFragment implements
		OnClickListener {

	EditText mHeartRate;
	Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnCls;
	InputHeartRateBPMDialogListener mListener;

	public interface InputHeartRateBPMDialogListener {
		public void onInputHRBpmDialogOK(String bpm);

		public void onInputHRDialogCancel();
	}

	/**
	 * Factory method to create new instance of this fragment using the provided
	 * parameters.
	 * 
	 * @return A new instance of fragment EnterHeartRateBPMDialog.
	 */
	public static InputHeartRateBPMDialog newInstance() {
		return new InputHeartRateBPMDialog();
	}

	public InputHeartRateBPMDialog() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		try {
			mListener = (InputHeartRateBPMDialogListener) getTargetFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException("Calling fragment must implement "
					+ "EnterHeartRateBPMDialogListener interface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View view = inflater.inflate(
				R.layout.dialog_fragment_enter_heart_rate_bpm, null);
		final AlertDialog.Builder builder = new AlertDialog.Builder(
				getActivity());

		mHeartRate = (EditText) view.findViewById(R.id.et_heart_rate_bpm);
		mHeartRate.setText("");

		btn0 = (Button) view.findViewById(R.id.btn0);
		btn1 = (Button) view.findViewById(R.id.btn1);
		btn2 = (Button) view.findViewById(R.id.btn2);
		btn3 = (Button) view.findViewById(R.id.btn3);
		btn4 = (Button) view.findViewById(R.id.btn4);
		btn5 = (Button) view.findViewById(R.id.btn5);
		btn6 = (Button) view.findViewById(R.id.btn6);
		btn7 = (Button) view.findViewById(R.id.btn7);
		btn8 = (Button) view.findViewById(R.id.btn8);
		btn9 = (Button) view.findViewById(R.id.btn9);
		btnCls = (Button) view.findViewById(R.id.btnCLEAR);

		btn0.setOnClickListener(this);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);
		btn7.setOnClickListener(this);
		btn8.setOnClickListener(this);
		btn9.setOnClickListener(this);
		btnCls.setOnClickListener(this);

		builder.setView(view)
				.setTitle("Enter Heart Rate")
				.setNegativeButton("CANCEL",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (mListener != null)
									mListener.onInputHRDialogCancel();
							}
						})
				.setPositiveButton("PROCEED",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								String heartRate = mHeartRate.getText()
										.toString().trim();

								if (heartRate.length() != 0) {
									if (mListener != null)
										mListener
												.onInputHRBpmDialogOK(mHeartRate
														.getText().toString()
														.trim());
								} else {
									Toast.makeText(getActivity(),
											"Please enter a heart rate",
											Toast.LENGTH_SHORT).show();
								}
							}
						});

		return builder.create();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCLEAR:
			mHeartRate.setText("");
			break;
		default:
			String value = getResources().getResourceEntryName(v.getId());
			mHeartRate.append(value.substring(value.length() - 1));
			break;
		}
	}
}
