package com.ismat.neorate.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ismat.neorate.R;
import com.ismat.neorate.model.Quiz;

/**
 * {@link DialogFragment} that allows users to set up quizzes
 * 
 */
public class QuizSetupDialog extends DialogFragment {

    private static final String ARG_TIMER_MODE = "timer_mode";
    private String mTimerMode;
	private QuizSetupDialogListener mListener;


	public interface QuizSetupDialogListener {
		public void onQuizSetupDialogClick(int assessmentTypeId,
				int assessmentCountId);
	}

	/**
	 * Factory method to create new instance of this fragment using the provided
	 * parameters.
	 * 
	 * @return A new instance of fragment {@link QuizSetupDialog}.
	 */
	public static QuizSetupDialog newInstance(String timerMode) {
//		return new QuizSetupDialog();

        QuizSetupDialog fragment = new QuizSetupDialog();

        Bundle args = new Bundle();
        args.putString(ARG_TIMER_MODE, timerMode); // clock, timer, both
        fragment.setArguments(args);

        return fragment;
	}

	public QuizSetupDialog() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

        if (getArguments() != null) {
            mTimerMode = getArguments().getString(ARG_TIMER_MODE);
        }

		try {
			mListener = (QuizSetupDialogListener) getTargetFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException("Calling fragment must implement "
					+ "QuizSetupDialogListener interface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);

		LayoutInflater inflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View view = inflater.inflate(R.layout.dialog_fragment_quiz_setup,
				null);

        RadioButton btn1 = (RadioButton) view.findViewById(R.id.rbtn_1);
        RadioButton btn8 = (RadioButton) view.findViewById(R.id.rbtn_8);


        // Show correct options
        if (mTimerMode == Quiz.TIMER_MODE_RANDOM) {
            // Hide all others
            btn1.setVisibility(View.GONE);
            view.findViewById(R.id.rbtn_5).setVisibility(View.GONE);
            view.findViewById(R.id.rbtn_10).setVisibility(View.GONE);
            view.findViewById(R.id.rbtn_15).setVisibility(View.GONE);
            // Set default checked
            btn8.setChecked(true);
        } else {
            // Hide timer mode values
            btn8.setVisibility(View.GONE);
            view.findViewById(R.id.rbtn_12).setVisibility(View.GONE);
            view.findViewById(R.id.rbtn_16).setVisibility(View.GONE);
            // Set default checked
            btn1.setChecked(true);
        }


		final RadioGroup rgCount, rgType;
		rgCount = (RadioGroup) view.findViewById(R.id.rg_assessment_count);
		rgType = (RadioGroup) view.findViewById(R.id.rg_assessment_type);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Quiz Setup")
				.setView(view)
				.setPositiveButton("DONE",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// // Get user choices
								if (mListener != null) {
									mListener.onQuizSetupDialogClick(
											rgType.getCheckedRadioButtonId(),
											rgCount.getCheckedRadioButtonId());
								}
							}
						});
		return builder.create();
	}
}
