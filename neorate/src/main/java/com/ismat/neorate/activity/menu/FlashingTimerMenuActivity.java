package com.ismat.neorate.activity.menu;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.SixSecondFlashingTimerActivity;
import com.ismat.neorate.activity.app.TwoSecondFlashingTimerActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;

/**
 * This activity displays the menu that allows the user to pick one of the
 * two timers
 * 
 */
@SuppressLint("NewApi")
public class FlashingTimerMenuActivity extends ListActivity {

	private ListMenuAdapter mListMenuAdapter;
	String mTitles[] = { "Two Second Timer", "Six Second Timer" };
	String mDescriptions[] = {
			"Make the screen flash every two seconds! (Newborn Resuscitation)",
			"Make the screen flash every six seconds! (Heart Rate Assessement)" };

	Integer mIcons[] = { R.drawable.two, R.drawable.six };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mListMenuAdapter = new ListMenuAdapter(this, mTitles, mDescriptions,
				mIcons);
		setListAdapter(mListMenuAdapter);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			// actionBar.setTitle(getString(R.string.app_name));
			// actionBar.setSubtitle(this.getClass().getSimpleName());
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = null;
		switch (position) {
		case 0:
			intent = new Intent(FlashingTimerMenuActivity.this,
					TwoSecondFlashingTimerActivity.class);
			break;
		case 1:
			intent = new Intent(FlashingTimerMenuActivity.this,
					SixSecondFlashingTimerActivity.class);
			break;
		default:
			break;
		}
		startActivity(intent);
	}
}
