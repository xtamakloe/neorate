package com.ismat.neorate.activity.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ismat.neorate.R;
import com.ismat.neorate.fragment.QuizFragment;
import com.ismat.neorate.fragment.QuizFragment.OnFragmentInteractionListener;
import com.ismat.neorate.model.Quiz;

/**
 * This activity loads and starts the quiz fragment using a digital clock
 * 
 */
public class ClockQuizActivity extends FragmentActivity implements
		OnFragmentInteractionListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_clock_quiz);

		if (findViewById(R.id.clock_method_fragment_container) != null) {
			if (savedInstanceState != null) {
				return;
			}

			// Create fragment for quiz using digital clock in quiz mode
			Intent intent = getIntent();
			String quizMode = intent.getStringExtra("quizMode");
			QuizFragment fragment = QuizFragment.newInstance(quizMode, Quiz.TIMER_MODE_CLOCK);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.clock_method_fragment_container, fragment)
					.commit();
		}
	}

	@Override
	public void onFragmentInteraction(Uri uri) {
	}
}
