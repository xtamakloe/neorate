package com.ismat.neorate.activity.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.menu.MainMenuActivity;
import com.ismat.neorate.fragment.SessionFragment;
import com.ismat.neorate.fragment.SignInFragment.OnFragmentInteractionListener;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.helper.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
@SuppressLint("NewApi")
public class SessionActivity extends FragmentActivity implements
		OnFragmentInteractionListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_session);

		// TODO: Use styling to accomplish this
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			getActionBar().hide();
		}

		FragmentManager fm = getSupportFragmentManager();
		Fragment fg = fm.findFragmentById(R.id.session_fragment_container);

		if (fg == null) {
			FragmentTransaction ft = fm.beginTransaction();
			ft.add(R.id.session_fragment_container,
					SessionFragment.newInstance());
			ft.commit();
		}
	}

	@Override
	public void beginSession(SessionManager sessionMgr, String username,
			String token, long userId) {
		sessionMgr.createLoginSession("Name", username, token, userId);
		Intent i = new Intent(getApplicationContext(), MainMenuActivity.class);
		startActivity(i);
		finish();
	}

	/**
	 * Shows the progress UI and hides the signup form.
	 */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show, final View loginView,
			final View progressView) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			loginView.setVisibility(show ? View.GONE : View.VISIBLE);
			loginView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							loginView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			progressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							progressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			loginView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	@Override
	public boolean validateForm(TextView usernameView, TextView passwordView) {
		// Reset errors.
		usernameView.setError(null);
		passwordView.setError(null);

		// Store values at the time of the login attempt.
		String username = usernameView.getText().toString();
		String password = passwordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			passwordView.setError(getString(R.string.error_invalid_password));
			focusView = passwordView;
			cancel = true;
		}

		// Check for a valid username
		if (TextUtils.isEmpty(username)) {
			usernameView.setError(getString(R.string.error_field_required));
			focusView = usernameView;
			cancel = true;
		} 
		// else if (!isEmailValid(username)) {
		// usernameView.setError(getString(R.string.error_invalid_username));
		// focusView = usernameView;
		// cancel = true;
		// }

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
			return false;
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			return true;
		}
	}

	public boolean isEmailValid(String email) {
		// TODO: Replace this with your own logic
		return email.contains("@");
	}

	public boolean isPasswordValid(String password) {
		// TODO: Replace this with your own logic
		return password.length() > 4;
	}
}
