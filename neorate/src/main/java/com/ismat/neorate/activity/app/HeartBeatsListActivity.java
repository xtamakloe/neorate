package com.ismat.neorate.activity.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.adapter.HeartBeatsListAdapter;
import com.ismat.neorate.helper.HeartBeatPlayer;

import java.util.ArrayList;
import java.util.List;

public class HeartBeatsListActivity extends ActionBarActivity {

    private ListView mListView;
    private List<String> mLabels;
    private HeartBeatPlayer mPlayer;
    private TextView mNowPlayingLabel;
    private TextView mStatusLabel;
    private TextView mBPMLabel;
//    private ImageButton mBtnPlay;
//    private LinearLayout mBtnPlayLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart_beats_list);

        // Set up player
        mPlayer = new HeartBeatPlayer(getApplicationContext(), null);

        mLabels = new ArrayList<String>();
        for (int i = 50; i < 155; i += 5) {
            mLabels.add(Integer.toString(i));
        }

//        mBtnPlay = (ImageButton) findViewById(R.id.btn_play_heart_beat);
//        mBtnPlayLayout = (LinearLayout) findViewById(R.id.ll_play_heart_beat);
        mListView = (ListView) findViewById(R.id.lv_heartbeats);
        mNowPlayingLabel = (TextView) findViewById(R.id.tv_now_playing);
        mStatusLabel = (TextView) findViewById(R.id.tv_play_status);
        mBPMLabel = (TextView) findViewById(R.id.tv_bpm_label);

//        mBtnPlay.setVisibility(View.GONE);
        mBPMLabel.setVisibility(View.INVISIBLE);
        mNowPlayingLabel.setVisibility(View.INVISIBLE);

//        mBtnPlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Set label to last played
//                mStatusLabel.setText("Last Played:");
//                mPlayer.pauseSoundFX();
//                mBtnPlayLayout.setVisibility(View.GONE);
//                mNowPlayingLabel.setVisibility(View.INVISIBLE);
//            }
//        });

        mListView.setAdapter(new HeartBeatsListAdapter(this, mLabels));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get selected heart rate
                String heartRate = (String) parent.getItemAtPosition(position);

                // Stop currently playing heart rate
                mPlayer.stopSoundFX();

                // Play selected heart rate
                mPlayer.playSoundFX(Integer.parseInt(heartRate));

                // Set labels
                mStatusLabel.setText("Now Playing:");
                mNowPlayingLabel.setText(heartRate);
                mBPMLabel.setVisibility(View.VISIBLE);
                mNowPlayingLabel.setVisibility(View.VISIBLE);
//                mBtnPlayLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlayer.stopSoundFX();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPlayer.stopSoundFX();
    }
}
