package com.ismat.neorate.activity.menu;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.HelpMenuActivity;
import com.ismat.neorate.activity.app.ImagePagerViewActivity;
import com.ismat.neorate.activity.app.RandomQuizActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;
import com.ismat.neorate.helper.SessionManager;

import java.util.ArrayList;

/**
 * This activity displays the home screen of the application which is the main
 * menu
 */
public class MainMenuActivity extends ListActivity {
    private SessionManager sessionManager;
    private ListMenuAdapter mMenuAdapter;
    private ArrayList<Integer> mPresentationImages;

    String mTitles[] = {"Newborn Resuscitation", "Using NeoRate",
            "Training Zone", "Random Quiz", "Flashing Timers", "My Progress",
            "Logout"};

    String mDescriptions[] = {"Summary of newborn resuscitation guidance",
            "How to use the app", "Learn to use the timer",
            "Test yourself with all the methods",
            "Use the 2 second or 6 second timers alone",
            "Monitor your progress using the app", "Log out of app"};

    Integer mIcons[] = {R.drawable.news, R.drawable.questionmark,
            R.drawable.book, R.drawable.random, R.drawable.lightbulb,
            R.drawable.progress, R.drawable.logout};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check for user session
        sessionManager = new SessionManager(this);
        sessionManager.checkLogin();

        // Create menu
        mMenuAdapter = new ListMenuAdapter(this, mTitles, mDescriptions, mIcons);
        setListAdapter(mMenuAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = null;
        switch (position) {
            case 0:
                // New born resuscitation slides
                intent = new Intent(MainMenuActivity.this, ImagePagerViewActivity.class);

                mPresentationImages = new ArrayList<Integer>();
                mPresentationImages.add(R.drawable.slide22);
                mPresentationImages.add(R.drawable.slide23);
                mPresentationImages.add(R.drawable.slide24);
                mPresentationImages.add(R.drawable.slide25);
                mPresentationImages.add(R.drawable.slide26);
                mPresentationImages.add(R.drawable.slide27);
                mPresentationImages.add(R.drawable.slide28);
                intent.putExtra("images", mPresentationImages);
                intent.putExtra("title", "Newborn Resuscitation");
                break;

            case 1:
                // Using neorate slides
                intent = new Intent(MainMenuActivity.this, HelpMenuActivity.class);
                break;

            case 2:
                // Training zone activity => leads to quiz activity and quiz section
                intent = new Intent(MainMenuActivity.this,
                        TrainingZoneMenuActivity.class);
                break;

            case 3:
                // Launch random quiz
                intent = new Intent(MainMenuActivity.this, RandomQuizActivity.class);
                intent.putExtra("quizMode", "quiz");
                break;

            case 4:
                // Flashing timers
                intent = new Intent(MainMenuActivity.this,
                        FlashingTimerMenuActivity.class);
                break;

            case 5:
                // User progress activity
                intent = new Intent(MainMenuActivity.this,
                        MyProgressMenuActivity.class);
                break;

            case 6:
                // Logout
                sessionManager.logoutUser();
                finish();
                break;

            default:
                break;
        }

        if (intent != null)
            startActivity(intent);
    }

}
