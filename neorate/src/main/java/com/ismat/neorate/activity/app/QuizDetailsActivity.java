package com.ismat.neorate.activity.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ismat.neorate.R;
import com.ismat.neorate.adapter.AssessmentsListAdapter;
import com.ismat.neorate.model.Quiz;

/**
 * This activity displays the details of a quiz including information on each
 * assessment
 * 
 */
public class QuizDetailsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz_details);
		if (savedInstanceState == null) {
			long quizId = getIntent().getExtras().getLong("quizId");
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.quiz_results_fragment_container,
							QuizDetailsFragment.newInstance(quizId)).commit();
		}
	}

	/**
	 * Fragment for quiz details shown in activity
	 */
	public static class QuizDetailsFragment extends Fragment {

		static final String ARG_QUIZ_ID = "quiz_id";
		private Quiz mQuiz;
        private LinearLayout mRandomQuizLayout;


        public QuizDetailsFragment() {
		}

		public static QuizDetailsFragment newInstance(long quizId) {
			QuizDetailsFragment fragment = new QuizDetailsFragment();
			Bundle args = new Bundle();
			args.putLong(ARG_QUIZ_ID, quizId);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			if (getArguments() != null) {
				mQuiz = Quiz.findById(getArguments().getLong(ARG_QUIZ_ID));
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_quiz_details,
					container, false);

			AssessmentsListAdapter adapter = new AssessmentsListAdapter(
					mQuiz.getAssessments(), getActivity());

            getActivity().setTitle(mQuiz.getTitle() + " Results");
            mRandomQuizLayout = (LinearLayout) rootView.findViewById(R.id.layout_random_quiz);

			// Set quiz details
			((TextView) rootView.findViewById(R.id.tv_quiz_mode))
					.setText("Mode: " + mQuiz.getMode());

			((TextView) rootView.findViewById(R.id.tv_quiz_assessment_mode))
					.setText("Assessed By: "
							+ mQuiz.getAssessmentValueType());

			((TextView) rootView.findViewById(R.id.tv_quiz_assessment_count))
					.setText("No. of Assessments: "
							+ Integer.toString(mQuiz.getAssessments().size()));

			((TextView) rootView.findViewById(R.id.tv_quiz_average_time))
					.setText("Average Time: " + mQuiz.getAverageTotalTimeInSeconds());

			((TextView) rootView.findViewById(R.id.tv_quiz_score))
					.setText(mQuiz.getTotalScore());

            // Random quiz
            if (mQuiz.isRandom()) {
                // Display clock and timer scores separately
                ((TextView) rootView.findViewById(R.id.tv_quiz_clock_score))
                        .setText(mQuiz.getClockScore());

                ((TextView) rootView.findViewById(R.id.tv_quiz_timer_score))
                        .setText(mQuiz.getTimerScore());

                ((TextView) rootView.findViewById(R.id.tv_quiz_clock_duration))
                        .setText(mQuiz.getAverageClockTimeInSeconds());

                ((TextView) rootView.findViewById(R.id.tv_quiz_timer_duration))
                        .setText(mQuiz.getAverageTimerTimeInSeconds());
            } else {
                // Hide layout
                mRandomQuizLayout.setVisibility(View.GONE);
            }


			((ListView) rootView.findViewById(R.id.lv_assessments))
					.setAdapter(adapter);

			return rootView;
		}
	}
}