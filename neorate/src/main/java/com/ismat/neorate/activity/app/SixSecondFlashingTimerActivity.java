package com.ismat.neorate.activity.app;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ismat.neorate.R;
import com.ismat.neorate.helper.FlashingTimer;

/**
 * This activity displays the six second flashing timer 
 * 
 */
@SuppressLint("NewApi")
public class SixSecondFlashingTimerActivity extends Activity {

	private RelativeLayout mLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//        // Hide title bar
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		setContentView(R.layout.activity_six_second_flasher);

		mLayout = (RelativeLayout) findViewById(R.id.SixSecondFlasherLayout);
		mLayout.setBackgroundColor(Color.BLACK);
		FlashingTimer flasher = new FlashingTimer(
				SixSecondFlashingTimerActivity.this, 6, mLayout, true, false);
		flasher.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

}