package com.ismat.neorate.activity.app;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.ismat.neorate.R;
import com.ismat.neorate.adapter.FragmentPagerAdapter;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.util.ArrayList;

/**
 * This activity simulates a slideshow effect by displaying images in screen
 * fragments. 
 * 
 */
public class ImagePagerViewActivity extends FragmentActivity {

	private ViewPager mViewPager;
	private ArrayList<Integer> itemData;
	private FragmentPagerAdapter adapter;
	PageIndicator mIndicator;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_pager_view);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

		mViewPager = (ViewPager) findViewById(R.id.viewPager);
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);

		// Get images for slide
		itemData = getIntent().getIntegerArrayListExtra("images");

		// Set title
		setTitle(getIntent().getCharSequenceExtra("title"));

		adapter = new FragmentPagerAdapter(getSupportFragmentManager(),
				itemData);
		mViewPager.setAdapter(adapter);

		mIndicator.setViewPager(mViewPager);
	}
}