package com.ismat.neorate.activity.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ismat.neorate.R;
import com.ismat.neorate.fragment.QuizFragment;
import com.ismat.neorate.fragment.QuizFragment.OnFragmentInteractionListener;
import com.ismat.neorate.model.Quiz;

/**
 * This activity loads and starts the quiz fragment using the flashing timer
 * 
 */
public class FlashingTimerQuizActivity extends FragmentActivity implements
		OnFragmentInteractionListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flashing_timer_quiz);

		if (findViewById(R.id.flashing_timer_quiz_fragment_container) != null) {
			if (savedInstanceState != null) {
				return;
			}

			// Create fragment for six second technique quiz
			Intent intent = getIntent();
			String quizMode = intent.getStringExtra("quizMode");
            QuizFragment fragment = QuizFragment.newInstance(quizMode, Quiz.TIMER_MODE_FLASHING_TIMER);
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.flashing_timer_quiz_fragment_container, fragment)
					.commit();
		}
	}

	@Override
	public void onFragmentInteraction(Uri uri) {
	}
}
