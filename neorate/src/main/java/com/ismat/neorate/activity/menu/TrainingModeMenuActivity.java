package com.ismat.neorate.activity.menu;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.ClockQuizActivity;
import com.ismat.neorate.activity.app.FlashingTimerQuizActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;

/**
 * This activity displays the menu for selecting a particular method used to
 * assess a quiz in training mode
 * 
 */
@SuppressLint("NewApi")
public class TrainingModeMenuActivity extends ListActivity {

	String mTrainingModes[] = { "Clock", "Six Second Technique" };

	String mDescriptions[] = { "Practice using a digital clock",
			"Practice using a flashing timer" };

	Integer mIcons[] = { R.drawable.timer, R.drawable.lightbulb };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new ListMenuAdapter(this, mTrainingModes, mDescriptions,
				mIcons));

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		Intent myIntent = null;
		switch (position) {
		case 0:
			myIntent = new Intent(TrainingModeMenuActivity.this,
					ClockQuizActivity.class);
			break;

		case 1:
			myIntent = new Intent(TrainingModeMenuActivity.this,
					FlashingTimerQuizActivity.class);
			break;

		default:
			break;
		}
		myIntent.putExtra("quizMode", "training");
		startActivity(myIntent);
	}
}
