package com.ismat.neorate.activity.app;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;

import com.ismat.neorate.R;
import com.ismat.neorate.helper.FlashingTimer;

/**
 * This activity displays the two second flashing timer 
 * 
 */
@SuppressLint("NewApi")
public class TwoSecondFlashingTimerActivity extends Activity {

	private LinearLayout mLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//        // Hide title bar
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		setContentView(R.layout.activity_two_second_flasher);

		mLayout = (LinearLayout) findViewById(R.id.TwoSecondFlasherLayout);
		mLayout.setBackgroundColor(Color.BLACK);
		
		// Initialize flashing timer 
		FlashingTimer flasher = new FlashingTimer(
				TwoSecondFlashingTimerActivity.this, 2, mLayout, true, false);
		
		// Start flashing timer 
		flasher.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

}