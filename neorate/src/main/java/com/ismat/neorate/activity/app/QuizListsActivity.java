package com.ismat.neorate.activity.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ismat.neorate.R;
import com.ismat.neorate.fragment.QuizListFragment;
import com.ismat.neorate.model.Quiz;

/**
 * This activity handles display of all quiz types (categorical, actual) and
 * quiz modes (timer, no timer, 6s): Timer quizzes, Clock quizzes, 6s flasher
 * quizzes
 * 
 * @author Christian Tamakloe
 * 
 */
public class QuizListsActivity extends ActionBarActivity implements
		ActionBar.TabListener {
	String mQuizMode = null;

	SectionsPagerAdapter mSectionsPagerAdapter;
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz_lists);

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
			}
		});

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {

			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

		// Set title based on quiz type
		mQuizMode = getIntent().getStringExtra(
				getString(R.string.intent_key_quiz_mode));

		if (mQuizMode.equals(Quiz.TIMER_MODE_CLOCK)) {
			setTitle("Digital Clock Quizzes");
		} else if (mQuizMode.equals(Quiz.TIMER_MODE_FLASHING_TIMER)) {
			setTitle("6s Timer Quizzes");
        } else if (mQuizMode.equals(Quiz.TIMER_MODE_RANDOM)) {
            setTitle("Random Quizzes");
		} else
			setTitle("None");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz_lists, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
	}

	/**
	 * Adapter to load data for quiz lists
	 *
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			// NB: Only assessmentType set here because mode has already been
			// set by calling activity 
			switch (position) {
			case 0:
				// Show quizzes assessed by actual BPM values
				fragment = QuizListFragment.newInstance(mQuizMode,
						Quiz.ASSESSMENT_ACTUAL);
				break;

			case 1:
				// Show quizzes assessed by BPM categories 
				fragment = QuizListFragment.newInstance(mQuizMode,
						Quiz.ASSESSMENT_CATEGORICAL);
				break;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return "ACTUAL";

			case 1:
				return "CATEGORICAL";

			default:
				return null;
			}
		}
	}
}
