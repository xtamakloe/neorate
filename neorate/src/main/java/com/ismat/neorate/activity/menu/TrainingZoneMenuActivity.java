package com.ismat.neorate.activity.menu;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.HeartBeatsListActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;

/**
 * Displays a menu that allows users to select between quiz and training modes
 * 
 */
@SuppressLint("NewApi")
public class TrainingZoneMenuActivity extends ListActivity {

	private ListMenuAdapter mListMenuAdapter;

	String mMenuTitles[] = { "Training", "Quiz", "Heart Rates" };
	String mMenuDescriptions[] = { "Improve your skills through practice",
			"Listen to a heartbeat and try to guess the correct heart rate",
            "Choose a heart rate and listen to how it sounds"};

	Integer mIcons[] = { R.drawable.clock, R.drawable.quiz, R.drawable.stethoscope };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mListMenuAdapter = new ListMenuAdapter(this, mMenuTitles,
				mMenuDescriptions, mIcons);
		setListAdapter(mListMenuAdapter);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = null;
		switch (position) {
		case 0:
			intent = new Intent(TrainingZoneMenuActivity.this,
					TrainingModeMenuActivity.class);
			break;
		case 1:
			intent = new Intent(TrainingZoneMenuActivity.this,
					QuizModeMenuActivity.class);
			break;
        case 2:
            intent = new Intent(TrainingZoneMenuActivity.this,
                    HeartBeatsListActivity.class);
            break;
		default:
			break;
		}
		startActivity(intent);
	}
}
