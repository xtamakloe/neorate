package com.ismat.neorate.activity.app;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ismat.neorate.R;
import com.ismat.neorate.adapter.ListMenuAdapter;

import java.util.ArrayList;

/**
 * This activity that displays the help section of the application
 * 
 */
@SuppressLint("NewApi")
public class HelpMenuActivity extends ListActivity {
	private ArrayList<Integer> mSlideImages;

	String mTitles[] = { "Six Second Timer", "Two Second Timer" };
	String mDescriptions[] = { "Using the 6s timer", "Using the 2s timer" };

	Integer mIcons[] = { R.drawable.six, R.drawable.two };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new ListMenuAdapter(this, mTitles, mDescriptions, mIcons));
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(HelpMenuActivity.this, ImagePagerViewActivity.class);
		mSlideImages = new ArrayList<Integer>();
		switch (position) {
		case 0:
			mSlideImages.add(R.drawable.slide2);
			mSlideImages.add(R.drawable.slide3);
			mSlideImages.add(R.drawable.slide4);
			mSlideImages.add(R.drawable.slide5);
			mSlideImages.add(R.drawable.slide6);
			mSlideImages.add(R.drawable.slide7);
			mSlideImages.add(R.drawable.slide8);
			mSlideImages.add(R.drawable.slide9);
			mSlideImages.add(R.drawable.slide10);
			mSlideImages.add(R.drawable.slide11);
			mSlideImages.add(R.drawable.slide12);
			mSlideImages.add(R.drawable.slide13);
			intent.putExtra("title", "Using the 6 second timer");
			break;
		case 1:
			mSlideImages.add(R.drawable.slide15);
			mSlideImages.add(R.drawable.slide16);
			mSlideImages.add(R.drawable.slide17);
			mSlideImages.add(R.drawable.slide18);
			mSlideImages.add(R.drawable.slide19);
			mSlideImages.add(R.drawable.slide20);
			intent.putExtra("title", "Using the 2 second timer");
			break;
		}
		intent.putExtra("images", mSlideImages);
		startActivity(intent);
	}
}
