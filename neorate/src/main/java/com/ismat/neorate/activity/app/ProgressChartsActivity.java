package com.ismat.neorate.activity.app;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ismat.neorate.R;
import com.ismat.neorate.dialog.ChartSetupDialog;
import com.ismat.neorate.dialog.ChartSetupDialog.ChartSetupDialogListener;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.helper.Utility;
import com.ismat.neorate.model.Quiz;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.List;

/**
 * This activity displays user quiz information in a chart
 * @author kojo
 *
 */
public class ProgressChartsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_progress_charts);
		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.charts_fragment_container,
							ProgressChartsFragment.newInstance()).commit();
		}
	}

	/**
	 * Fragment for use in this activity
	 * 
	 */
	public static class ProgressChartsFragment extends Fragment implements
			ChartSetupDialogListener, OnClickListener {

		private static final int CHART_SETUP_DIALOG = 0;

		private LinearLayout mGraphView;
		private SessionManager mSessionManager;
		private GraphicalView mChart;

		private XYMultipleSeriesRenderer mChartRenderer;

		public ProgressChartsFragment() {
		}

		public static ProgressChartsFragment newInstance() {
			return new ProgressChartsFragment();
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			mSessionManager = new SessionManager(getActivity());
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Initialize chart settings
			initializeChart();

			// Show dialog for user to make chart choices
			showChartSetupDialog();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_progress_charts,
					container, false);
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);

			// Generate chart button
			((Button) view.findViewById(R.id.btn_generate_chart))
					.setOnClickListener(this);
			mGraphView = (LinearLayout) view.findViewById(R.id.ll_graph_view);
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_generate_chart:
				// Generate a new chart
				showChartSetupDialog();
				break;
			// case R.id.btn_close_chart:
			// // Exit activity
			// getActivity().finish();
			// break;
			}
		}

		private void showChartSetupDialog() {
			DialogFragment fragment = ChartSetupDialog.newInstance();
			// Set callback to this fragment from the dialogFragment
			fragment.setTargetFragment(this, CHART_SETUP_DIALOG);
			fragment.show(getActivity().getSupportFragmentManager(),
					"chart_setup");
		}

		@Override
		public void onChartSetupDialogClick(int quizCount, String assessmentType) {
			new GenerateGraphViewTask().execute(Integer.toString(quizCount),
					assessmentType);
		}

		private void initializeChart() {
			// Create XYSeriesRenderer to customize XSeries

			XYSeriesRenderer renderer1 = new XYSeriesRenderer();
			renderer1.setColor(Color.BLUE);
			renderer1.setPointStyle(PointStyle.DIAMOND);
			renderer1.setDisplayChartValues(true);
			renderer1.setLineWidth(2);
			renderer1.setFillPoints(true);

			XYSeriesRenderer renderer2 = new XYSeriesRenderer();
			renderer2.setColor(Color.RED);
			renderer2.setPointStyle(PointStyle.CIRCLE);
			renderer2.setDisplayChartValues(true);
			renderer2.setLineWidth(2);
			renderer2.setFillPoints(true);

			// Create XYMultipleSeriesRenderer to customize the whole chart
			mChartRenderer = new XYMultipleSeriesRenderer();

			mChartRenderer.setXTitle("Quizzes");
			mChartRenderer.setYTitle("Score (%)");
			// mRenderer.setZoomButtonsVisible(true);
			// mRenderer.setXLabels(0);
			mChartRenderer.setPanEnabled(false);
			mChartRenderer.setShowGrid(true);
			// mRenderer.setClickEnabled(true);

			mChartRenderer.setMargins(new int[] { 110, 120, 120, 30 });
			// mChartRenderer.setLegendHeight(5);
			mChartRenderer.setFitLegend(true);
			mChartRenderer.setLegendTextSize(40.0f);

			mChartRenderer.setAxisTitleTextSize(40.0f);
			mChartRenderer.setChartTitleTextSize(40.0f);
			mChartRenderer.setXAxisMin(0);
			mChartRenderer.setYAxisMin(0);
			mChartRenderer.setYAxisMax(100);
			mChartRenderer.setYLabelsPadding(40.0f);

			DisplayMetrics metrics = getActivity().getResources()
					.getDisplayMetrics();
			float fontSize = TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_SP, 18, metrics);
			mChartRenderer.setLabelsTextSize(fontSize);
			renderer1.setChartValuesTextSize(fontSize);
			renderer2.setChartValuesTextSize(fontSize);

			// Adding the XSeriesRenderer to the MultipleRenderer.
			mChartRenderer.addSeriesRenderer(renderer1);
			mChartRenderer.addSeriesRenderer(renderer2);
		}

		class GenerateGraphViewTask extends
				AsyncTask<String, Void, XYMultipleSeriesDataset> {

			@Override
			protected XYMultipleSeriesDataset doInBackground(String... params) {
				int quizCount = Integer.parseInt(params[0]);
				String assessmentType = params[1];

				String title = "Last " + quizCount + " quizzes assessed using "
						+ assessmentType + " values";

				String userId = Long.toString(mSessionManager
						.getLoggedInUserId());
				
				mChartRenderer.setXAxisMax(quizCount + 1);
				mChartRenderer.setChartTitle(title);

				// Get quizzes
				List<Quiz> flasherList = Quiz.afindAll(userId, Quiz.TIMER_MODE_FLASHING_TIMER,
						assessmentType, true, quizCount);

				List<Quiz> clockList = Quiz.afindAll(userId, Quiz.TIMER_MODE_CLOCK,
						assessmentType, true, quizCount);

				XYSeries flasherSeries = generateSeries(Quiz.TIMER_MODE_FLASHING_TIMER,
						flasherList);
				XYSeries clockSeries = generateSeries(Quiz.TIMER_MODE_CLOCK,
						clockList);

				// Create a Dataset to hold the XSeries.
				XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

				// Add X series to the Dataset.
				dataset.addSeries(flasherSeries);
				dataset.addSeries(clockSeries);

				return dataset;
			}

			@Override
			protected void onPostExecute(XYMultipleSeriesDataset dataset) {
				super.onPostExecute(dataset);

				mGraphView.removeView(mChart);

				// Creating an intent to plot line chart using dataset and
				// multipleRenderer
				mChart = (GraphicalView) ChartFactory.getLineChartView(
						getActivity(), dataset, mChartRenderer);

				mGraphView.addView(mChart);
			}

			private XYSeries generateSeries(String mode, List<Quiz> quizList) {
				int dataSize = quizList.size();
				String title = null;

				if (mode.equals(Quiz.TIMER_MODE_FLASHING_TIMER))
					title = "Flashing Timer";

				if (mode.equals(Quiz.TIMER_MODE_CLOCK))
					title = "Clock";

				XYSeries series = new XYSeries(title + " Quizzes");

				for (int i = 0; i < dataSize; i++) {
					Double x, y;
					x = (double) (i + 1);

					String percentage = quizList.get(i).getScorePercentage();

					y = Double.parseDouble(Utility.round(
							Float.valueOf(percentage), 0));

					series.add(x, y);
				}

				return series;
			}

		}

	}
}
