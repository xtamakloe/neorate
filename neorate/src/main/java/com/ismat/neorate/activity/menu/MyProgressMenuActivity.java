package com.ismat.neorate.activity.menu;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.ProgressChartsActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;
import com.ismat.neorate.helper.SessionManager;
import com.ismat.neorate.model.Quiz;
import com.ismat.neorate.model.User;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * This activity displays the menu options for displaying user progress
 * information. It displays an extra menu item that when an admin user is logged
 * in that allows all user information stored in the application to be exported
 * 
 */
public class MyProgressMenuActivity extends ListActivity {

	private SessionManager mSessionManager;
	private ListMenuAdapter mListMenuAdapter;
	final static String EXPORT_ALL_TITLE = "Export All Data";
	final static String EXPORT_ALL_DESCRIPTION = "Download all data to SD card";
	final static int EXPORT_ALL_ICON = R.drawable.download;

	List<String> mTitles;
	List<String> mDescriptions;
	List<Integer> mIcons;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTitles = new LinkedList<String>();
		mTitles.add("Quiz Results");
		mTitles.add("Progress Report");
		mTitles.add("Export Data");

		mDescriptions = new LinkedList<String>();
		mDescriptions.add("View previous quiz results");
		mDescriptions.add("Compare your progress using both techniques");
		mDescriptions.add("Save your data to SD card");

		mIcons = new LinkedList<Integer>();
		mIcons.add(R.drawable.list4);
		mIcons.add(R.drawable.chart);
		mIcons.add(R.drawable.export);

		mSessionManager = new SessionManager(this);

		// Add admin menu item if admin is logged in
		if (mSessionManager.isAdminSession()) {
			mTitles.add("Export All Data");
			mDescriptions.add("Save data from all users to SD Card");
			mIcons.add(R.drawable.download);
		}

		mListMenuAdapter = new ListMenuAdapter(this,
				mTitles.toArray(new String[mTitles.size()]),
				mDescriptions.toArray(new String[mDescriptions.size()]),
				mIcons.toArray(new Integer[mIcons.size()]));
		setListAdapter(mListMenuAdapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = null;
		boolean exportSuccessful = false;

		switch (position) {
		case 0:
			intent = new Intent(MyProgressMenuActivity.this,
					MyQuizzesMenuActivity.class);
			break;

		case 1:
			intent = new Intent(MyProgressMenuActivity.this,
					ProgressChartsActivity.class);
			break;

		case 2:
			exportSuccessful = exportUserData(false);
			break;

		case 3:
			exportSuccessful = exportUserData(true);
			break;

		default:
			break;
		}

		if (position == 2 || position == 3) {
			if (exportSuccessful) {
				Toast.makeText(this, "Data exported successfully",
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "Error exporting data...",
						Toast.LENGTH_SHORT).show();
			}
		}

		if (intent != null)
			startActivity(intent);
	}

	boolean exportUserData(boolean exportAll) {

		boolean success = false;
		String queryId, userName;

		if (exportAll) {
			queryId = null;
			userName = "ALL-USERS";
		} else {
			SessionManager sMgr = new SessionManager(this);
			HashMap<String, String> user = sMgr.getUserDetails();

			queryId = user.get(SessionManager.KEY_USER_ID);
			userName = user.get(SessionManager.KEY_USERNAME).split("@")[0];
		}

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm")
				.format(new Date());

		String csvFile = android.os.Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/NeoRateData_"
				+ userName
				+ "_"
				+ timeStamp + ".csv";

		List<Quiz> quizzes = Quiz.findAll(queryId, null, null);

		// Populate CSV data
		List<String[]> csvData = new ArrayList<String[]>();

		// Add headers
		csvData.add(new String[] { "User ID", "Username", "Quiz Number",
				"Quiz Score (Percent)", "Quiz Average Time(s)" });

		// Add data
		for (Quiz quiz : quizzes) {
			User user = quiz.user;
			csvData.add(new String[] { Long.toString(user.getId()),
					user.getUsername(), quiz.getNumber(),
					quiz.getScoreFraction(), quiz.getAverageTotalTimeInSeconds() });
		}

		// Write to file
		CSVWriter writer = null;
		try {
			writer = new CSVWriter(new FileWriter(csvFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		writer.writeAll(csvData);

		try {
			writer.close();
			success = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return success;
	}
}
