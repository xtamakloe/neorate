package com.ismat.neorate.activity.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ismat.neorate.R;
import com.ismat.neorate.fragment.QuizFragment;
import com.ismat.neorate.model.Quiz;


public class RandomQuizActivity extends FragmentActivity
        implements QuizFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_quiz);

        if (findViewById(R.id.random_quiz_fragment_container) != null) {
            if (savedInstanceState != null)
                return;

            // Create fragment for quiz with random timers
            Intent intent = getIntent();
            String quizMode = intent.getStringExtra("quizMode");
            QuizFragment fragment = QuizFragment.newInstance(quizMode, Quiz.TIMER_MODE_RANDOM);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.random_quiz_fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
