package com.ismat.neorate.activity.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.menu.MainMenuActivity;

/**
 * This activity displays a splash screen for use at the start of the
 * application
 * 
 * 
 */
public class SplashScreenActivity extends Activity {
	private static final int SPLASH_DISPLAY_TIME = 1000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent intent = new Intent();
				intent.setClass(SplashScreenActivity.this,
						MainMenuActivity.class);

				SplashScreenActivity.this.startActivity(intent);
				SplashScreenActivity.this.finish();

				overridePendingTransition(R.animator.activityfadein,
						R.animator.splashfadeout);
			}

		}, SPLASH_DISPLAY_TIME);
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}