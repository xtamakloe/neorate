package com.ismat.neorate.activity.menu;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.ismat.neorate.R;
import com.ismat.neorate.activity.app.QuizListsActivity;
import com.ismat.neorate.adapter.ListMenuAdapter;
import com.ismat.neorate.model.Quiz;

/**
 * Displays a menu that allows users to select which quizzes they would like to
 * view
 * 
 */
public class MyQuizzesMenuActivity extends ListActivity {

	private ListMenuAdapter mListMenuAdapter;

	String mTitles[] = { "Clock", "Flashing Timer", "Random" };

	String mDescriptions[] = { "View Results for Clock Quizzes",
            "View Results for Timer Quizzes", "View Results for Random Quizzes" };

	Integer mIcons[] = { R.drawable.timer, R.drawable.lightbulb, R.drawable.random };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mListMenuAdapter = new ListMenuAdapter(this, mTitles, mDescriptions,
				mIcons);
		setListAdapter(mListMenuAdapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(MyQuizzesMenuActivity.this,
                QuizListsActivity.class);

		switch (position) {
		case 0:
			// List of 6s flasher quizzes
			// Set only mode here because QuizListsActivity sets assessmentType
			intent.putExtra(getString(R.string.intent_key_quiz_mode),
					Quiz.TIMER_MODE_CLOCK);
			break;

		case 1:
			// List of timer quizzes
			// Set only mode here because QuizListsActivity sets assessmentType
			intent.putExtra(getString(R.string.intent_key_quiz_mode),
					Quiz.TIMER_MODE_FLASHING_TIMER);
			break;

		case 2:
            // List random quizzes
            // Set only mode here because QuizListsActivity sets assessmentType
            intent.putExtra(getString(R.string.intent_key_quiz_mode),
                    Quiz.TIMER_MODE_RANDOM);
            break;

		default:
			break;
		}

		if (intent != null)
			startActivity(intent);
	}
}
