package com.ismat.neorate.model;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.ismat.neorate.helper.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Models a quiz taken by a user
 * 
 * 
 */
@Table(name = "quizzes")
public class Quiz extends Model {

	public static final String TIMER_MODE_CLOCK = "clock";
    public static final String TIMER_MODE_FLASHING_TIMER = "6s_timer";
    public static final String TIMER_MODE_RANDOM = "random";
    public static final String TIMER_MODE_NO_CLOCK = "no_clock";


	public static final String ASSESSMENT_ACTUAL = "actual";
	public static final String ASSESSMENT_CATEGORICAL = "category";

	// Quiz number
	@Column(name = "number", index = true)
	public String number;

	// User who took the quiz
	@Column(name = "user", index = true)
	public User user;

	// Type of quiz: six_second, timer, no_timer
	@Column(name = "mode", index = true)
	public String mode;

	// Types of assessment: category or actual
	@Column(name = "assessment_type")
	public String assessmentType;

	// Timestamp
	@Column(name = "timestamp", index = true)
	private Date timestamp;

	/** Utility Methods */

	// Returns list of assessments in the quiz
//	public List<Assessment> getAssessments() {
//		return getMany(Assessment.class, "quiz");
//	}

    public From allAssessments() {
        return new Select().from(Assessment.class).
                where(Cache.getTableName(Assessment.class) + "." + "quiz" + "=?", getId());
    }

    public List<Assessment> getAssessments() {
        return allAssessments().execute();
    }

    public List<Assessment> getTimerAssessments() {
        return allAssessments().where("timer_mode=?", TIMER_MODE_FLASHING_TIMER).execute();
    }

    public List<Assessment> getClockAssessments() {
        return allAssessments().where("timer_mode=?", TIMER_MODE_CLOCK).execute();
    }


	public static Quiz findById(long id) {
		return new Select().from(Quiz.class).where("id = ?", id)
				.executeSingle();
	}
	
	public static Quiz findLatestRecords(long id, int count) {
		return new Select().from(Quiz.class).where("id = ?", id).limit(5)
				.executeSingle();
	}
	
	// TODO: get [quizCount] last [assessmentType] quizzes for two
	// [modes]

	

	public static List<Quiz> afindAll(String userId, String quizMode,
			String assessmentType, boolean ascending, int count) {

		From items = new Select().from(Quiz.class);

		if (userId != null)
			items.where("user = ?", userId);

		if (quizMode != null)
			items.where("mode = ?", quizMode);

		if (assessmentType != null)
			items.where("assessment_type = ?", assessmentType);

		if (ascending) {
			items.orderBy("timestamp ASC");
		} else {
			items.orderBy("timestamp DESC");
		}
		
		if (count > -1) 
			items.limit(count); 
		
		return items.execute(); 		
	}
	
	
	public static List<Quiz> findAll(String userId, String quizMode,
			String assessmentType) {

		From items = new Select().from(Quiz.class);

		if (userId != null)
			items.where("user = ?", userId);

		if (quizMode != null)
			items.where("mode = ?", quizMode);

		if (assessmentType != null)
			items.where("assessment_type = ?", assessmentType);

		return items.orderBy("timestamp ASC").execute();
	}

	public void setCreatedTimestamp(String date) {
		SimpleDateFormat sf = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss ZZZZZ yyyy");
		sf.setLenient(true);
		try {
			this.timestamp = (Date) sf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// TODO: Fix locale
	/**
	 * Returns date of creation of quiz
	 * 
	 * @return
	 */
	public String getCreatedTimestamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",
				Locale.getDefault());
		// Date date = new Date();
		return dateFormat.format(this.timestamp);
	}

	public String getNumber() {
		return this.number;
	}

	public String getTitle() {
		return "Quiz " + getNumber();
	}

	/**
	 * Returns quiz mode
	 * 
	 * @return
	 */
	public String getMode() {
		if (this.mode.equals(TIMER_MODE_FLASHING_TIMER)) {
			return "6s Flasher";
		} else if (this.mode.equals(TIMER_MODE_CLOCK)) {
			return "Clock";
		} else if (this.mode.equals(TIMER_MODE_RANDOM)) {
			return "Random";
        } else if (this.mode.equals(TIMER_MODE_NO_CLOCK)) {
            return "No Timer";
		} else {
			return "UNKOWN Quiz Mode";
		}
	}

	/**
	 * Returns assessment mode
	 * 
	 * @return
	 */
	public String getAssessmentValueType() {
		if (this.assessmentType.equals(ASSESSMENT_ACTUAL)) {
			return "Actual BPM";
		} else if (this.assessmentType.equals(ASSESSMENT_CATEGORICAL)) {
			return "Category";
		} else {
			return "UnKnown Assessment Value Type";
		}
	}

	public String getAverageAccuracy() {
		Float total = 0f;
		List<Assessment> assessmentsList = getAssessments();
		for (Assessment assessment : assessmentsList) {
			total += Float.parseFloat(assessment.getError());
		}
		return Float.toString((total / (float) assessmentsList.size()));
	}

	public String getAverageTotalTimeInSeconds() {
//		long total = 0;
//		List<Assessment> assessmentsList = getAssessments();
//        for (Assessment assessment : assessmentsList) {
//			total += (assessment.timeElapsed);
//		}
//		long averageTime = total / assessmentsList.size();
//		return Utility.formatTime(averageTime);
        List<Assessment> assessments = getAssessments();
        return printTime(getTotalElapsedTime(assessments), assessments);
	}

    public String getAverageClockTimeInSeconds() {
        List<Assessment> assessments = getClockAssessments();
        return printTime(getTotalElapsedTime(assessments), assessments);
    }

    public String getAverageTimerTimeInSeconds() {
        List<Assessment> assessments = getTimerAssessments();
        return printTime(getTotalElapsedTime(assessments), assessments);
    }

    public Long getTotalElapsedTime(List<Assessment> assessments) {
        long total = 0;
        for (Assessment assessment : assessments) {
            total += (assessment.timeElapsed);
        }
        return total;
    }

    public String printTime(long totalAssessmentsTime, List<Assessment> assessments) {
        long averageTime = totalAssessmentsTime / assessments.size();
        return Utility.formatTime(averageTime);
    }

	public String getTotalScore() {
        List<Assessment> assessments =  getAssessments();
        return printScore(getSuccessfulCount(assessments), assessments);
	}

    public String getTimerScore() {
        List<Assessment> assessments =  getTimerAssessments();
        return printScore(getSuccessfulCount(assessments), assessments);
    }

    public String getClockScore() {
        List<Assessment> assessments =  getClockAssessments();
        return printScore(getSuccessfulCount(assessments), assessments);
    }

    public String printScore(int successfulAssessmentCount, List<Assessment> assessments) {
        return Integer.toString(successfulAssessmentCount) + "/"
                + Integer.toString(assessments.size());
    }

    public int getSuccessfulCount(List<Assessment> assessments) {
        int successful = 0;
        for (Assessment assessment : assessments) {
            if (assessment.isSuccessful())
                successful++;
        }
        return successful;
    }

	public String getScoreFraction() {
		return Float.toString(scoreFraction());
	}
	
	public String getScorePercentage() {
		return Float.toString(scoreFraction() * 100);
	}
	
	private Float scoreFraction() {
		String[] score = getTotalScore().split("/");
		float percent = Float.parseFloat(score[0]) / Float.parseFloat(score[1]);
		return percent; 
	}

	public boolean isAccuracyAssessed() {
		return this.assessmentType.equals(Quiz.ASSESSMENT_ACTUAL);
	}

	public boolean isCategoryAssessed() {
		return this.assessmentType.equals(Quiz.ASSESSMENT_CATEGORICAL);
	}

    public boolean isRandom() {
        return this.mode.equals(TIMER_MODE_RANDOM);
    }
}