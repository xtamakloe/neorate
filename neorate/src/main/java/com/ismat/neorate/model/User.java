package com.ismat.neorate.model;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Models a user of the app. Two types of users: admins and standard users
 * 
 * 
 */
@Table(name = "users")
public class User extends Model {

	// User's role
	@Column(name = "is_admin")
	public boolean isAdmin;

	// User's username
	@Column(name = "username", index = true)
	public String username;

	@Column(name = "encrypted_password")
	private String encrypted_password;

	// Returns list of quizzes taken by user
	public List<Quiz> quizzes() {
		return getMany(Quiz.class, "quizzes");
	}
	
	public boolean isAdmin() {
		return this.isAdmin; 
	}
	
	public String getUsername() {
		return this.username; 
	}

	/**
	 * Virtual attribute, sets user's encrypted_password field after hashing
	 * password
	 * 
	 * @param password
	 *            String to hash for encrypted_password
	 */
	public void setPassword(String password) {
		this.encrypted_password = computeSHAHash(password);
	}

	/**
	 * Check if a password is valid for a user
	 * 
	 * @param password
	 *            password to check
	 * @return True if password is valid
	 * 
	 */
	public boolean isValidPassword(String password) {
		return this.encrypted_password.equals(computeSHAHash(password));
	}

	/**
	 * Computes a SHA1 hash
	 * 
	 * @param password
	 *            String to hash
	 * @return Returns a SHA1 hash based on string provided
	 */
	public String computeSHAHash(String password) {
		String hash = null;
		MessageDigest mdSha1 = null;
		try {
			mdSha1 = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e1) {
			Log.e("com.ismat.neorate.model.User",
					"Error initializing SHA1 message digest");
		}
		try {
			mdSha1.update(password.getBytes("ASCII"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		byte[] data = mdSha1.digest();

		try {
			hash = convertToHex(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hash;
	}

	@SuppressLint("NewApi")
	private static String convertToHex(byte[] data) throws java.io.IOException {
		final int NO_OPTIONS = 0;
		StringBuffer sb = new StringBuffer();
		String hex = null;
		hex = Base64.encodeToString(data, 0, data.length, NO_OPTIONS);
		sb.append(hex);
		return sb.toString();
	}

	/* FINDERS */

	public static User findByUsername(String username) {
		return new Select().from(User.class).where("username = ?", username)
				.executeSingle();
	}

	public static User findById(long id) {
		return new Select().from(User.class).where("id = ?", id)
				.executeSingle();
	}

}