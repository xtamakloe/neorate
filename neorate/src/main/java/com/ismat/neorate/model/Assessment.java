package com.ismat.neorate.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.ismat.neorate.helper.Utility;

/**
 * Models the two types of assessments that can be taken in a quiz i.e. Category
 * and Accuracy assessments
 * 
 * 
 */
@Table(name = "assessments")
public class Assessment extends Model {

	/** Common fields */
	// Assessment number
	@Column(name = "number", index = true)
	public int number;

	// Quiz assessment is part of
	@Column(name = "quiz", index = true)
	public Quiz quiz;

	// Type of assessment, either "category" or "actual"
	@Column(name = "type", index = true)
	public String type;

    @Column(name = "timer_mode")
    public String timerMode;

	// Time taken to provide answer
	@Column(name = "time_elapsed")
	public long timeElapsed;

	/** Fields for Accuracy assessments */
	// Correct bpm
	@Column(name = "correct_bpm")
	public int correctBPM;

	// BPM provided as answer
	@Column(name = "answer_bpm")
	public int answerBPM;

	/** Fields for Category assessments */
	// Correct category
	@Column(name = "correct_category")
	public String correctCategory;

	// Category provided as answer
	@Column(name = "answer_category")
	public String answerCategory;

	/** Properties */

	public String getNumber() {
		return Integer.toString(this.number);
	}

	public String getDuration() {
		return Utility.formatTime(timeElapsed);
	}

	public String getCorrectAnswer() {
		return Integer.toString(this.correctBPM) + " BPM";
	}

	public String getUsersAnswer() {
		if (isCategorical()) {
			return toCategory(this.answerCategory);
		}
		if (isActual()) {
			return Integer.toString(this.answerBPM) + " BPM";
		}
		return null;
	}

	public String getError() {
		return Utility.round(calculateError(), 0);
	}

	public boolean isSuccessful() {
		// Categorical assessments are successful in the same category
		if (isCategorical()) {
			if (this.correctCategory.equals(this.answerCategory))
				return true;
		}
		// Actual values are successful if user not more than 10% off
		if (isActual()) {
			if (calculateError() < 10)
				return true;
		}
		return false;
	}

	public boolean isActual() {
		return this.type.equals(Quiz.ASSESSMENT_ACTUAL);
	}

	public boolean isCategorical() {
		return this.type.equals(Quiz.ASSESSMENT_CATEGORICAL);
	}

    public boolean isTimerAssessment() {
        return this.timerMode.equals(Quiz.TIMER_MODE_FLASHING_TIMER);
    }

    public boolean isClockAssessment() {
        return this.timerMode.equals(Quiz.TIMER_MODE_CLOCK);
    }


    /** Utility */

	public static String toCategory(String cat) {
		if (cat.equals("_60")) {
			return "Below 60";
		} else if (cat.equals("60_100")) {
			return "60 - 100";
		} else if (cat.equals("100_")) {
			return "Over 100";
		}
		return "Unkown Category";
	}

	private float calculateError() {
		if (isActual()) {
			return Utility.calculateError(this.correctBPM, this.answerBPM);
		}
		return 0;
	}
}